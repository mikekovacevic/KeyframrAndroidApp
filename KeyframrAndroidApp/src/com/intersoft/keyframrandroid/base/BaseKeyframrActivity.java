/*-------------------------------------------------------------------------*/ 
/* Program Name : BaseKeyframrActivity.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments : Base Activity that nearly all other activities inherit from. Includes common methods and variables used
 * by all activities. See each function below for more details. */ 
/*-------------------------------------------------------------------------*/

package com.intersoft.keyframrandroid.base;

import it.custom.printer.api.android.CustomAndroidAPI;
import it.custom.printer.api.android.CustomException;
import it.custom.printer.api.android.CustomPrinter;

import org.json.JSONObject;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.codepath.libraries.androidviewhelpers.SimpleAlertDialog;
import com.codepath.libraries.androidviewhelpers.SimpleProgressDialog;
import com.intersoft.keyframrandroid.R;
import com.intersoft.keyframrandroid.activity.LoginActivity;
import com.intersoft.keyframrandroid.activity.SettingsActivity;
import com.intersoft.keyframrandroid.requests.LogoutRequest;
import com.intersoft.keyframrandroid.spiced.SpicedRoboSherlockFragmentActivity;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

public class BaseKeyframrActivity extends SpicedRoboSherlockFragmentActivity{
	private SharedPreferences userPreferences;
	private SharedPreferences serverPreferences;
	private SharedPreferences printerPreferences;
	private String URL;
	protected BluetoothAdapter bluetooth;
	private String AuthToken;
	private String PrinterMacAddress;
	private int UserId;
	protected static CustomPrinter printer;
	public SimpleProgressDialog pd;
	
	// User Shared Preferences. Used to store Auth Token for current User.
	protected SharedPreferences getUserPrefs(){
		return this.userPreferences;
	}
	// Server Shared Preferences. Used to store Server URL (for database and backend)
	protected SharedPreferences getServerPrefs(){
		return this.serverPreferences;
	}
	// Printer Shared Preferences. Used to store Printer mac address.
	protected SharedPreferences getPrinterPrefs(){
		return this.printerPreferences;
	}
	
	public String getServerURL(){
		return URL = serverPreferences.getString("url", "");
	}
	
	public String getAuthToken(){
		return AuthToken = userPreferences.getString("AuthToken", "");
	}
	
	public String getMacAddress(){
		return PrinterMacAddress = printerPreferences.getString("macaddress", "");
	}
	
	public int getUserId(){
		return UserId = userPreferences.getInt("UserId", 0);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		userPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);
		serverPreferences = getSharedPreferences("ServerUrl", MODE_PRIVATE);
		printerPreferences = getSharedPreferences("Printer", MODE_PRIVATE);
		
		PrinterMacAddress = printerPreferences.getString("macaddress", "");
		URL = serverPreferences.getString("url", "");
		AuthToken = userPreferences.getString("AuthToken", "");
		UserId = userPreferences.getInt("UserId", 0);
		setupBluetooth();
		pd = SimpleProgressDialog.build(this);
	}
	public static void setPrinter(CustomPrinter p){
		printer = p;
	}
	
	public static CustomPrinter getPrinter(){
		return printer;
	}
	// Set-up options. All activities will have the same options, unless overridden.
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getSupportMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	public void reconnectPrinter(){
		BluetoothDevice bDevice = bluetooth.getRemoteDevice(PrinterMacAddress);
		openDevice(bDevice);
	}
    //Open the printer if it isn't already opened
    public boolean openDevice(BluetoothDevice selected)
    {
    	//If i changed the device
    	if (printer != null)
    	{

			try
            {
				//Force close
				printer.close();
            }
			catch(CustomException e )
            {
            	
            	e.printStackTrace();
        		return false;
            }
			catch(Exception e )
            {
    			//Show error
				e.printStackTrace();
    			return false;
            }
			printer = null;
    	}
    	
    	//If i never open it
    	if (printer == null)
    	{
 
    			try {
    				printer = new CustomAndroidAPI().getPrinterDriverBT(selected);
					return true;
				} catch (CustomException e) {
					e.printStackTrace();
					return false;
				}

    	}
    	//Already opened
    	return true;
    	
    }    
	// Setup options functionality.
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    int itemId = item.getItemId();
	    if(itemId == R.id.action_logout){
			logOut();
			return true;
		} else if(itemId == android.R.id.home){
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}else {
			return super.onOptionsItemSelected(item);
		}
	}
	// Set-up bluetooth adapter
	protected void setupBluetooth(){
		bluetooth = BluetoothAdapter.getDefaultAdapter();
		
		if (!bluetooth.isEnabled()) {
		    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
		    startActivityForResult(enableBtIntent, 1);
		}
	}
	// Create a request to server to destroy user session (and log out).
	private void logOut(){
		LogoutRequest logoutRequest = new LogoutRequest(URL, AuthToken);
		getSpiceManager().execute(logoutRequest, new LogoutRequestListener());		
	}
	
	@Override
	public void onResume() {
	    super.onResume();
	    
	    if (!userPreferences.getString("AuthToken", "").equals("")) {
	        
	    } else {
	        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
	        startActivity(intent);
	    }

	    PrinterMacAddress = printerPreferences.getString("macaddress", "");
    	if(!PrinterMacAddress.isEmpty()){
    		reconnectPrinter();
			try {
				if(printer == null || (printer != null && !printer.isPrinterOnline())){
					showAlertMsg("Printer Offline", "Please turn on paired printer and make sure it is in range.");
				}
			} catch (CustomException e) {
				showAlertMsg("Printer Offline", "Please turn on paired printer and make sure it is in range.");
				e.printStackTrace();
			} catch(NullPointerException e){
				showAlertMsg("Printer Offline", "Please turn on paired printer and make sure it is in range.");
				e.printStackTrace();
			}
			
    	}
    	else if( this.getClass() != SettingsActivity.class){
    		SimpleAlertDialog sa = SimpleAlertDialog.build(this, "Please Choose a Printer to Connect", new SimpleAlertDialog.SimpleAlertListener(){

				@Override
				public void onPositive() {
					Intent in = new Intent(BaseKeyframrActivity.this, SettingsActivity.class);
					startActivity(in);
				}
	
				@Override
				public void onNegative() {
					// TODO Auto-generated method stub
					
				}
			});
    		
    		sa.setAlertButtonText("Printer Set-up", null);
    		sa.show();
		}
	}
	// Listener for log out request
	public final class LogoutRequestListener implements RequestListener< JSONObject > {

        @Override
        public void onRequestFailure( SpiceException spiceException ) {
            Toast.makeText( getApplicationContext(), "Could not log out.", Toast.LENGTH_SHORT ).show();

        }

        @Override
        public void onRequestSuccess( final JSONObject result ) {
            
            SharedPreferences.Editor editor = userPreferences.edit();
            // Clear the auth token and user Id
			editor.putString("AuthToken", "");
			editor.remove("UserId");
			editor.commit();
            
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finish();
            
        }
    }
	// Show a simple alert dialog
    public void showAlertMsg(String title,String msg)
	{
    	AlertDialog.Builder dialogBuilder;    	
		dialogBuilder = new AlertDialog.Builder(this);    	
    	
		dialogBuilder.setNeutralButton( "OK", new DialogInterface.OnClickListener() 
		{			
			public void onClick(DialogInterface dialog, int which) {				
				dialog.dismiss();				
			}
		});
		
		dialogBuilder.setTitle(title);    	
		dialogBuilder.setMessage(msg);    	
		dialogBuilder.show();
    	
	}  
 
}
