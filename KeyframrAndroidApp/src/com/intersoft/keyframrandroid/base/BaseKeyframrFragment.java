/*-------------------------------------------------------------------------*/ 
/* Program Name : BaseKeyframrFragment.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments : Base Fragment that almost all fragments will inherit from. Allows fragments to access
 * Server URL and Auth Token. */ 
/*-------------------------------------------------------------------------*/

package com.intersoft.keyframrandroid.base;

import roboguice.RoboGuice;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.intersoft.keyframrandroid.spiced.SpicedRoboSherlockFragment;

public class BaseKeyframrFragment extends SpicedRoboSherlockFragment{
	protected SharedPreferences userPreferences;
	protected SharedPreferences serverPreferences;
	protected static String URL;
	protected static String AuthToken;
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        RoboGuice.getInjector(getActivity()).injectViewMembers(this);
        userPreferences = getActivity().getSharedPreferences("CurrentUser", 0);
        serverPreferences = getActivity().getSharedPreferences("ServerUrl", 0);
        URL = serverPreferences.getString("url","");
        AuthToken = userPreferences.getString("AuthToken","");
	}
	
	public BaseKeyframrActivity getKeyframrActivity(){
		return (BaseKeyframrActivity) getActivity();
	}
}
