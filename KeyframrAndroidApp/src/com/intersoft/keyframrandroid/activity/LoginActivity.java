/*-------------------------------------------------------------------------*/ 
/* Program Name : LoginActivity.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments : Activity for logging in to the server */ 
/*-------------------------------------------------------------------------*/

package com.intersoft.keyframrandroid.activity;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.codepath.libraries.androidviewhelpers.SimpleProgressDialog;
import com.intersoft.keyframrandroid.R;
import com.intersoft.keyframrandroid.fragment.ServerUrlDialogFragment;
import com.intersoft.keyframrandroid.requests.LoginRequest;
import com.intersoft.keyframrandroid.spiced.SpicedRoboSherlockFragmentActivity;
import com.intersoft.keyframrandroid.utils.Contents;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Activity which displays a login screen to the user
 */
@ContentView(R.layout.activity_login)
public class LoginActivity extends SpicedRoboSherlockFragmentActivity implements ServerUrlDialogFragment.OnCompleteListener {
	public static String URL;
	private SimpleProgressDialog pd;

	// Values for email and password at the time of the login attempt.
	private String mEmail;
	private String mPassword;
	private SharedPreferences mPreferences;
	private SharedPreferences serverPreferences;
	// UI references.
	
	private @InjectView(R.id.login_status_message) TextView mLoginStatusMessageView;
	private @InjectView(R.id.login_status) View mLoginStatusView;
	private @InjectView(R.id.login_form) View mLoginFormView;
	private @InjectView(R.id.password) EditText mPasswordView;
	private @InjectView(R.id.email) EditText mEmailView;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setupBluetooth();
		
		mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);
		
		serverPreferences = getSharedPreferences("ServerUrl", MODE_PRIVATE);
		URL = serverPreferences.getString("url", "");
		// Find Server URL stored in shared preferences. If it is empty, start a dialog fragment and prompt user to 
		// enter URL.
		if(URL.isEmpty()){
		  	FragmentManager fm = getSupportFragmentManager();
		  	ServerUrlDialogFragment serverUrlDialog = ServerUrlDialogFragment.newInstance(false);
		  	serverUrlDialog.show(fm, "fragment_server_url");
		}	
		else
			Toast.makeText(this, URL, Toast.LENGTH_LONG).show();
		
		mPasswordView
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_NULL) {
							attemptLogin();
							return true;
						}
						return false;
					}
				});


		findViewById(R.id.sign_in_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						attemptLogin();
					}
				});
		

	}
	
	private void setupBluetooth(){
		BluetoothAdapter bluetooth = BluetoothAdapter.getDefaultAdapter();
		
		if (!bluetooth.isEnabled()) {
		    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
		    startActivity(enableBtIntent);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getSupportMenuInflater().inflate(R.menu.login, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    int itemId = item.getItemId();
	    if(itemId == R.id.action_change_server){
			FragmentManager fm = getSupportFragmentManager();
		  	ServerUrlDialogFragment serverUrlDialog = ServerUrlDialogFragment.newInstance(true);
		  	serverUrlDialog.show(fm, "fragment_server_url");
			return true;
		}else {
			return super.onOptionsItemSelected(item);
		}
	}
	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {

		// Reset errors.
		mEmailView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		mEmail = mEmailView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 4) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(mEmail)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		} 

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			pd = SimpleProgressDialog.build(this, getString(R.string.login_progress_signing_in));
			pd.show();
			
			LoginRequest loginRequest = new LoginRequest(URL, mEmail, mPassword);
			getSpiceManager().execute(loginRequest, new LoginRequestListener());

		}
	}
	private void loginSuccess(){

		Toast.makeText( this, "Logged in successfully.", Toast.LENGTH_SHORT ).show();
		Intent intent = new Intent(this, TasksActivity.class);
        startActivity(intent);
		pd.dismiss();
	}
	public final class LoginRequestListener implements RequestListener< JSONObject > {

        @Override
        public void onRequestFailure( SpiceException spiceException ) {
        	pd.dismiss();
            Toast.makeText( LoginActivity.this, "Could not log in.", Toast.LENGTH_SHORT ).show();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        }

        @Override
        public void onRequestSuccess( final JSONObject result ) {
            
            SharedPreferences.Editor editor = mPreferences.edit();
            // save the returned auth_token into
            // the SharedPreferences
            try {
				editor.putString("AuthToken", "Token token="+result.getJSONObject("data").getString("auth_token"));
				editor.putInt("UserId", result.getJSONObject("data").getInt("user_id"));
			} catch (JSONException e) {
				e.printStackTrace();
			}
            editor.commit();
            
        	loginSuccess();
            // launch the HomeActivity and close this one
            
        }
    }

	@Override
	public void onComplete(String url) {
		URL = url;
		Toast.makeText(this, URL, Toast.LENGTH_LONG).show();
	}
	
}
