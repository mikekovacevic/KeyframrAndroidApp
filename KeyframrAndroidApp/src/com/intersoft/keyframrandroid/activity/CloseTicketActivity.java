/*-------------------------------------------------------------------------*/ 
/* Program Name : CloseTicketActivity.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments : Activity that handles closing of ticket */ 
/*-------------------------------------------------------------------------*/

package com.intersoft.keyframrandroid.activity;

import it.custom.printer.api.android.CustomException;
import it.custom.printer.api.android.CustomPrinter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.codepath.libraries.androidviewhelpers.SimpleAlertDialog;
import com.google.zxing.client.android.CaptureActivity;
import com.intersoft.keyframrandroid.R;
import com.intersoft.keyframrandroid.base.BaseKeyframrActivity;
import com.intersoft.keyframrandroid.models.json.Ticket;
import com.intersoft.keyframrandroid.requests.InvoiceCreateRequest;
import com.intersoft.keyframrandroid.requests.TicketUpdateRequestBuilder;
import com.intersoft.keyframrandroid.utils.Contents;
import com.intersoft.keyframrandroid.utils.ExpandAnimation;
import com.intersoft.keyframrandroid.utils.TicketStatus;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

@ContentView(R.layout.activity_close_ticket)
public class CloseTicketActivity extends BaseKeyframrActivity {
	private String ticket_data;
	private int ticket_id;
	private int customer_id;
	private int event_id;
	private Ticket currentTicket;
	private float amountPaid;
	@InjectView(R.id.ticketOwner) private TextView ticketOwnerView;
	@InjectView(R.id.ticketPrice) private TextView ticketPriceView;
	@InjectView(R.id.ticketType) private TextView ticketTypeView;
	@InjectView(R.id.invoiceSection) private LinearLayout invoiceSection;
	@InjectView(R.id.invoiceInfo) private LinearLayout invoiceInfo;
	@InjectView(R.id.invoiceButton) private ToggleButton toggleInvoice;
	@InjectView(R.id.printInvoice) private Button printButton;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		parseIntentExtras();
		setupActionBar();
		TicketUpdateRequestBuilder builder = new TicketUpdateRequestBuilder();
		if(event_id > -1){
			invoiceSection.setVisibility(View.GONE);
			printButton.setVisibility(View.GONE);
		}
//        toggleInvoice.setChecked(true);
        toggleInvoice.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				ExpandAnimation expandAni = new ExpandAnimation(invoiceInfo, 500);
	        	invoiceInfo.startAnimation(expandAni);
			}
		});
		
		builder.setCustomerId(customer_id)
			.setEventId(event_id)
			.setStatus(TicketStatus.VEHICLE_REQUESTED);
		getSpiceManager().execute(builder.build(getServerURL(), getAuthToken(), ticket_id), new VehicleRequestedListener());
	}
	
	private final class VehicleRequestedListener implements RequestListener< Ticket >{

		@Override
		public void onRequestFailure(SpiceException arg0) {
			Toast.makeText(CloseTicketActivity.this, arg0.getMessage(), Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onRequestSuccess(Ticket arg0) {
			Toast.makeText(CloseTicketActivity.this, "Vehicle Requested...", Toast.LENGTH_SHORT).show();
			currentTicket = arg0;
			amountPaid = arg0.getPrice();
			ticketOwnerView.setText(arg0.getParent()+" id: " + arg0.getParentId());
			ticketPriceView.setText("Price: " + arg0.getTicketType().getPrice());
			ticketTypeView.setText("Ticket Type: " + arg0.getTicketType().getName());
		}
		
	}
	private void parseIntentExtras() {
		Intent intent = this.getIntent();
		ticket_data = intent.getExtras().getString(Contents.Type.TICKET);
		ticket_id = extractID(ticket_data, Contents.Header.TICKET);
		customer_id = extractID(ticket_data, Contents.Header.CUSTOMER);
		event_id = extractID(ticket_data, Contents.Header.EVENT);
		
	}
	private int extractID(String data, String header){
		if (data.contains(header)){
			Pattern pt = Pattern.compile(header+"\\[([^\\]]+)]");
			Matcher mt = pt.matcher(data);
			if (mt.find()){
				return Integer.valueOf(mt.group(1));
			}
			
		}
		return -1;
	}
	
	public boolean checkPrinter(){
		SimpleAlertDialog sa = SimpleAlertDialog.build(this, "Printer Disconnected. Please Turn it on and try again.", new SimpleAlertDialog.SimpleAlertListener(){

			@Override
			public void onPositive() {
				return;
			}

			@Override
			public void onNegative() {
			}
		});
		
		sa.setAlertButtonText("Ok", null);
		
		try{
			reconnectPrinter();
			CustomPrinter prnDevice = getPrinter();
			prnDevice.isPrinterOnline();
		}
		catch(CustomException e){
			sa.show();
			e.printStackTrace();
			return false;
		}
		catch(NullPointerException e){
			sa.show();
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public void printInvoice(View view){
		if(!checkPrinter())
			return;
		InvoiceCreateRequest request = new InvoiceCreateRequest(getServerURL(), getAuthToken(), customer_id, ticket_id, amountPaid);
		getSpiceManager().execute(request, new InvoiceCreatedListener());
	}
	
	private final class InvoiceCreatedListener implements RequestListener< String >{

		@Override
		public void onRequestFailure(SpiceException arg0) {
			if( arg0.getCause().toString().contains("500")){
				printInvoice(currentTicket);
				Toast.makeText(CloseTicketActivity.this, "Ticket Successfully Paid", Toast.LENGTH_SHORT).show();
			}
			else
				Toast.makeText(CloseTicketActivity.this, arg0.getCause().toString(), Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onRequestSuccess(String arg0) {
			Toast.makeText(CloseTicketActivity.this, "Ticket Successfully Paid", Toast.LENGTH_SHORT).show();
			printInvoice(currentTicket);
		}
		
	}
	
	private void printInvoice(Ticket tick){
		float price = tick.getPrice();
		int parent_id = tick.getParentId();
		String parent = tick.getParent();
		String ticketType = tick.getTicketType().getName();
		
		CustomPrinter prnDevice = getPrinter();

		synchronized ("lockAccess") 
		{
			//***************************************************************************
			// PRINT PICTURE
			//***************************************************************************
			
	    	try
	        {
	    		//Print (Left Align and Fit to printer width)
	    		prnDevice.printTextLF(parent + " ID: " + parent_id);
	    		prnDevice.printTextLF("Ticket Type: " + ticketType);
	    		prnDevice.printTextLF("Price: " + price);
	        }
	    	catch(CustomException e )
	        {            	
	        	//Show Error
	    		e.printStackTrace();       		
	        }
			catch(Exception e )
	        {
				e.printStackTrace();
	        }

			//***************************************************************************
			// FEEDS and CUT
			//***************************************************************************
			
			try
	        {
				//Feeds (3)
				prnDevice.feed(3);
	    		//Cut (Total)
	    		prnDevice.cut(CustomPrinter.CUT_TOTAL);	    		
	        }
	    	catch(CustomException e )
	        {    
	    		//Only if isn't unsupported
	    		if (e.GetErrorCode() != CustomException.ERR_UNSUPPORTEDFUNCTION)
	    		{
	    			//Show Error
	    			e.printStackTrace();
	    		}
	        }
			catch(Exception e )
	        {
				e.printStackTrace();;
	        }
			
			//***************************************************************************
			// PRESENT
			//***************************************************************************
			
			try
	        {				
	    		//Present (40mm)
	    		prnDevice.present(40);
	        }
	    	catch(CustomException e )
	        {    
	    		//Only if isn't unsupported
	    		if (e.GetErrorCode() != CustomException.ERR_UNSUPPORTEDFUNCTION)
	    		{
	    			//Show Error
	    			e.printStackTrace();
	    		}
	        }
		}
	}
	
	public void scanVehicleTicket(View view){
		Intent intent = new Intent(this, CaptureActivity.class);
		startActivityForResult(intent, 0);
	}
	
	// Scan successful, pass results on to close ticket activity for parsing.
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == 0) {

			if (resultCode == RESULT_OK) {
				// code will be in the format of keyframr:ticket_id[3]vehicle_id[2]
				String scanContent = intent.getStringExtra("SCAN_RESULT").split(":")[1];
				// If the Ticket is verified as a vehicle ticket or a keys ticket, notify server that the vehicle has returned to
				// the customer
				if(verifyVehicleTicket(scanContent)){
					TicketUpdateRequestBuilder builder = new TicketUpdateRequestBuilder();
					builder.setCustomerId(customer_id)
						.setEventId(event_id)
						.setStatus(TicketStatus.VEHICLE_RETURNED);
					getSpiceManager().execute(builder.build(getServerURL(), getAuthToken(), ticket_id), new VehicleReturnedListener());
				}
			} else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(getApplicationContext(),
	        	        "No scan data received!", Toast.LENGTH_SHORT).show();
				}
		}
	}
	
	private final class VehicleReturnedListener implements RequestListener< Ticket >{

		@Override
		public void onRequestFailure(SpiceException arg0) {
			Toast.makeText(CloseTicketActivity.this, arg0.getMessage(), Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onRequestSuccess(Ticket arg0) {
			Toast.makeText(CloseTicketActivity.this, "Vehicle Returned...", Toast.LENGTH_SHORT).show();
		}
		
	}
	
	private boolean verifyVehicleTicket(String scanContent){
		if(extractID(scanContent,Contents.Header.TICKET) != ticket_id){
			Toast.makeText(this, "Ticket Id's do not match.", Toast.LENGTH_SHORT).show();
			return false;
		}
		if(scanContent.contains(Contents.Header.VEHICLE) || scanContent.contains(Contents.Header.KEYS))
			return true;
		else
			Toast.makeText(this, "Not a Vehicle Ticket. Please Scan the Vehicle or Keys Ticket.", Toast.LENGTH_SHORT).show();
		return false;
	}
	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

}
