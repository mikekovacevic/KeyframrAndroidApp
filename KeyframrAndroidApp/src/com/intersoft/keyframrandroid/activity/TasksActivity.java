/*-------------------------------------------------------------------------*/ 
/* Program Name : TasksActivity.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments : Activity that handles common tasks for a runner. Includes opening a ticket, scanning and closing a 
 * ticket. The settings activity can also be accessed from this activity, although it is hidden (see onOptionsItemSelected()) */ 
/*-------------------------------------------------------------------------*/

package com.intersoft.keyframrandroid.activity;

import roboguice.inject.ContentView;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.View;
import android.widget.Toast;

import com.actionbarsherlock.view.MenuItem;
import com.google.zxing.client.android.CaptureActivity;
import com.intersoft.keyframrandroid.R;
import com.intersoft.keyframrandroid.base.BaseKeyframrActivity;
import com.intersoft.keyframrandroid.utils.Contents;

@ContentView(R.layout.activity_main)
public class TasksActivity extends BaseKeyframrActivity{
	
	private int clickCount = 0;
	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		getActionBar().setHomeButtonEnabled(true);
	}
	
	@Override
	public void onResume(){
		super.onResume();
		clickCount = 0;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		int itemId = item.getItemId();	
		// We allow access to settings activity if the user clicks the home (top left corner of screen) 7 times.
		// This is just like enabling the developer options for any android device.
		if(itemId == android.R.id.home){
			clickCount++;
			if(clickCount > 6){
				Intent intent = new Intent(this, SettingsActivity.class);
				startActivity(intent);
			}
			return true;
		}else {
			return super.onOptionsItemSelected(item);
		}
	}
	public void createTicket(View view){
		Intent intent = new Intent(this, OpenTicketActivity.class);
		startActivity(intent);

	}
	// Start scan activity and expect a result	
	public void scanTicket(View view){
		Intent intent = new Intent(this, CaptureActivity.class);
		startActivityForResult(intent, 0);

	}
	// Scan successful, pass results on to close ticket activity for parsing.
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == 0) {

			if (resultCode == RESULT_OK) {
				// code will be in the format of keyframr:ticket_id[3]vehicle_id[2]
				String scanContent = intent.getStringExtra("SCAN_RESULT").split(":")[1];
				if(scanContent.contains(Contents.Header.CUSTOMER) || scanContent.contains(Contents.Header.EVENT)){      		
	        		Intent closeTicketIntent = new Intent(getBaseContext(), CloseTicketActivity.class);
	        		closeTicketIntent.putExtra(Contents.Type.TICKET, scanContent);
	        		startActivity(closeTicketIntent);
				}
				else
					Toast.makeText(this, "Please Scan a Customer Ticket.", Toast.LENGTH_SHORT).show();
			} else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(getApplicationContext(),
	        	        "No scan data received!", Toast.LENGTH_SHORT).show();
				}
		}
	}
	
	@Override
	public void onBackPressed() {
	}


}