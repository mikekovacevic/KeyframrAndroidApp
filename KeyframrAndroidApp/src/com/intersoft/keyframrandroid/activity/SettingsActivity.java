/*-------------------------------------------------------------------------*/ 
/* Program Name : SettingsActivity.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments : Activity that handles bluetooth set-up, connecting to a printer and saving the printer mac address */ 
/*-------------------------------------------------------------------------*/
package com.intersoft.keyframrandroid.activity;

import it.custom.printer.api.android.CustomException;
import it.custom.printer.api.android.PrinterFont;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.intersoft.keyframrandroid.R;
import com.intersoft.keyframrandroid.base.BaseKeyframrActivity;
import com.intersoft.keyframrandroid.fragment.PairedDevicesFragment;
import com.intersoft.keyframrandroid.utils.PairThread;

@ContentView(R.layout.activity_settings)
public class SettingsActivity extends BaseKeyframrActivity implements PairedDevicesFragment.OnCompleteListener
{

	static BluetoothDevice[] btDeviceList = null;
	private ArrayAdapter<String> mNewDevicesArrayAdapter;
	static ArrayAdapter<String> listAdapter;  
	static String deviceSelected = "";
	private String macAddress;
	private String lock = "lockAccess";
	private @InjectView(R.id.connectedDevice) TextView deviceView;
	private @InjectView(R.id.scanned_layout) LinearLayout scannedLayout;
	private @InjectView(R.id.scanned_dev_list) ListView newDevicesListView;
	private @InjectView(R.id.scan_button) Button scanButton;
	private @InjectView(R.id.loadingPanel) ProgressBar loadingPanel;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);    
        setupBluetooth();
        
        mNewDevicesArrayAdapter = new ArrayAdapter<String>(this, R.layout.list_item);
    	mNewDevicesArrayAdapter.setNotifyOnChange(true);
        newDevicesListView.setAdapter(mNewDevicesArrayAdapter);
        newDevicesListView.setOnItemClickListener(mDeviceClickListener);
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        this.registerReceiver(mReceiver, filter);

        // Register for broadcasts when discovery has finished
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        this.registerReceiver(mReceiver, filter);
        
    }    
    @Override
    public void onResume(){
    	super .onResume();
    	macAddress = getMacAddress();
    	if(!macAddress.isEmpty()){
    		BluetoothDevice bDevice = bluetooth.getRemoteDevice(macAddress);
    		deviceView.setText(bDevice.getName());	
    	}
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();	
        
        // Make sure we're not doing discovery anymore
        if (bluetooth != null) {
        	bluetooth.cancelDiscovery();
        }
         // Unregister broadcast listeners
        this.unregisterReceiver(mReceiver);
    }

	private void doDiscovery() {


        // If we're already discovering, stop it
        if (bluetooth.isDiscovering()) {
        	bluetooth.cancelDiscovery();
        }
         // Request discover from BluetoothAdapter
        bluetooth.startDiscovery();
    }
	// The on-click listener for all devices in the ListViews
    private OnItemClickListener mDeviceClickListener = new OnItemClickListener() {
        public void onItemClick(AdapterView<?> av, View v, int i, long l) {
            // Cancel discovery because it's costly and we're about to connect
            bluetooth.cancelDiscovery();
            // Get the device MAC address, which is the last 17 chars in the View
            String info = ((TextView) v).getText().toString();
            String address = info.substring(info.length() - 17);
            PairThread connection = new PairThread(SettingsActivity.this, bluetooth.getRemoteDevice(address), bluetooth);
            connection.run();
            if(connection.isPaired()){
    			scannedLayout.setVisibility(View.GONE);
    		}
        }
    };

    // The BroadcastReceiver that listens for discovered devices and
    // changes the title when discovery is finished
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // If it's already paired, skip it, because it's been listed already
                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                    mNewDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
                }
            // When discovery is finished, change the Activity title
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                if (mNewDevicesArrayAdapter.getCount() == 0) {
                    String noDevices = "No Nearby Devices Found...";
                    mNewDevicesArrayAdapter.add(noDevices);
                }
                loadingPanel.setVisibility(View.GONE);
                scanButton.setClickable(true);
                
            }
            
        }
    };
    // Fragment that displays paired devices.
    public void startPairedDevs(View view){
	  	FragmentManager fm = getSupportFragmentManager();
	  	PairedDevicesFragment pairedDevsDialog = PairedDevicesFragment.newInstance(deviceSelected);
	  	pairedDevsDialog.show(fm, "fragment_paired_devs");

    }
    // Start scanning for new devices
    public void startScan(View view){
    	mNewDevicesArrayAdapter.clear();

    	doDiscovery();
    	loadingPanel.setVisibility(View.VISIBLE);
    	view.setClickable(false);
    	scannedLayout.setVisibility(View.VISIBLE);
    }
    
    // Function to test printer and make sure it is connected properly.
    public void onPrintText(View view)  
	{  
    	PrinterFont fntPrinterNormal = new PrinterFont();
    	PrinterFont fntPrinterBold2X = new PrinterFont();
    	String strTextToPrint = "Testing printer...";
    	if (printer == null){
    		showAlertMsg("Error", "Printer is not connected");
    		return;
    	}
    	
    	try
        {
	    	//Fill class: NORMAL
	    	fntPrinterNormal.setCharHeight(PrinterFont.FONT_SIZE_X1);					//Height x1
	    	fntPrinterNormal.setCharWidth(PrinterFont.FONT_SIZE_X1);					//Width x1
	    	fntPrinterNormal.setEmphasized(false);										//No Bold
	    	fntPrinterNormal.setItalic(false);											//No Italic
	    	fntPrinterNormal.setUnderline(false);										//No Underline
	    	fntPrinterNormal.setJustification(PrinterFont.FONT_JUSTIFICATION_CENTER);	//Center
	    	fntPrinterNormal.setInternationalCharSet(PrinterFont.FONT_CS_DEFAULT);		//Default International Chars
	    	
	    	//Fill class: BOLD size 2X
	    	fntPrinterBold2X.setCharHeight(PrinterFont.FONT_SIZE_X2);					//Height x2
	    	fntPrinterBold2X.setCharWidth(PrinterFont.FONT_SIZE_X2);					//Width x2
	    	fntPrinterBold2X.setEmphasized(true);										//Bold
	    	fntPrinterBold2X.setItalic(false);											//No Italic
	    	fntPrinterBold2X.setUnderline(false);										//No Underline
	    	fntPrinterBold2X.setJustification(PrinterFont.FONT_JUSTIFICATION_CENTER);	//Center	    	
	    	fntPrinterBold2X.setInternationalCharSet(PrinterFont.FONT_CS_DEFAULT);		//Default International Chars	    	
        }
    	catch(CustomException e )
        {
        	
        	//Show Error
    		e.printStackTrace();
        }
    	catch(Exception e )
        {
    		e.printStackTrace();
        }
    	
    	//***************************************************************************
		// PRINT TEXT
		//***************************************************************************
    	    	
    	synchronized (lock) 
		{    		
	    	try
	        {
	    		//Print Text (NORMAL)
	    		printer.printTextLF(strTextToPrint, fntPrinterNormal);
	    		//Print Text (BOLD size 2X)
	    		printer.printTextLF(strTextToPrint, fntPrinterBold2X);
	    		printer.feed(2);
	        }
	    	catch(CustomException e )
            {            	
            	//Show Error
	    		e.printStackTrace();
            }
		}
	} 
    
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) 
    {
    	super.onSaveInstanceState(savedInstanceState);
		// Save UI state changes to the savedInstanceState.
		// This bundle will be passed to onCreate if the process is
		// killed and restarted.		        		
    }
    	
    
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) 
    {
    	super.onRestoreInstanceState(savedInstanceState);
    	//Restore UI state from the savedInstanceState.
    	// This bundle has also been passed to onCreate.
    	    	
    }
    

    // Once printer is selected from the fragment, we should connect to it (using the openDevice() func), and then, if 
    // successful, save the printer's mac address on the device for future use.
	@Override
	public void onComplete(BluetoothDevice selected) {
		if (openDevice(selected)){
			Toast.makeText(this, "Connected to " + selected.getName(), Toast.LENGTH_SHORT).show();
			// save mac address of the connected device
			getPrinterPrefs().edit().putString("macaddress", selected.getAddress()).commit();
			deviceSelected = selected.getName();
			deviceView.setText(deviceSelected);
		}
	}    
}