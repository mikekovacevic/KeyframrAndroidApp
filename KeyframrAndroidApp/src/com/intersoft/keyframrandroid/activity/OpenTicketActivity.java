/*-------------------------------------------------------------------------*/ 
/* Program Name : OpmTicketActivity.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments : Activity that handles opening of tickets. Multiple fragments are managed for that purpose. */ 
/*-------------------------------------------------------------------------*/

package com.intersoft.keyframrandroid.activity;

import it.custom.printer.api.android.CustomPrinter;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.google.inject.Inject;
import com.intersoft.keyframrandroid.R;
import com.intersoft.keyframrandroid.base.BaseKeyframrActivity;
import com.intersoft.keyframrandroid.fragment.TicketFragment.OnCompleteListener;
import com.intersoft.keyframrandroid.fragment.VehicleTicketFragment;
import com.intersoft.keyframrandroid.utils.TicketsPagerAdapter;

@ContentView(R.layout.activity_open_ticket)
public class OpenTicketActivity extends BaseKeyframrActivity implements OnCompleteListener{
	


	public static CustomPrinter prnDevice;
	private ActionBar actionBar;
	@InjectView(R.id.pager) ViewPager vPager;
	@Inject                 FragmentManager fm;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Show the Up button in the action bar.
		actionBar = getSupportActionBar();
		/** Set tab navigation mode */
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        /** Defining a listener for pageChange */
        ViewPager.SimpleOnPageChangeListener pageChangeListener = new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                actionBar.setSelectedNavigationItem(position);
            }
        };
        /** Setting the pageChange listener to the viewPager */
        vPager.setOnPageChangeListener(pageChangeListener);
 
        /** Creating an instance of FragmentPagerAdapter */
        TicketsPagerAdapter fragmentPagerAdapter = new TicketsPagerAdapter(fm);
 
        /** Setting the FragmentPagerAdapter object to the viewPager object */
        vPager.setAdapter(fragmentPagerAdapter);
 
        actionBar.setDisplayShowTitleEnabled(true);
 
        /** Defining tab listener */
        ActionBar.TabListener tabListener = new ActionBar.TabListener() {

			@Override
			public void onTabSelected(Tab tab, FragmentTransaction ft) {
				vPager.setCurrentItem(tab.getPosition());
			}

			@Override
			public void onTabUnselected(Tab tab, FragmentTransaction ft) {
				
			}

			@Override
			public void onTabReselected(Tab tab, FragmentTransaction ft) {
				
			}
        };
 
        /** Creating Customer Ticket Tab */
        Tab tab = actionBar.newTab()
                .setText("Customer Ticket")
                .setTabListener(tabListener);
 
        actionBar.addTab(tab);
 
        /** Creating Event Ticket Tab */
        tab = actionBar.newTab()
                .setText("Event Ticket")
                .setTabListener(tabListener);
 
        actionBar.addTab(tab);
		
	}

	// Once either of the customer or event ticket fragments completes, we have to start the vehicle ticket fragment.
	@Override
	public void onComplete(int ticket_id, int customer_id, int event_id) {
		VehicleTicketFragment vehFragment = VehicleTicketFragment.newInstance(ticket_id, customer_id, event_id);

		FragmentManager fm = getSupportFragmentManager();
		vehFragment.show(fm, "fragment_vehicle_ticket");
	}
	
}
