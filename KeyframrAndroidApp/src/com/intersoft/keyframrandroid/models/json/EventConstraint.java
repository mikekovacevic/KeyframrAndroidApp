package com.intersoft.keyframrandroid.models.json;

import com.google.api.client.util.Key;

public class EventConstraint {
	@Key private int id;
	@Key private TicketType ticket_type;
	@Key private int event_id;
	@Key private int amount;
	@Key private int amount_available;
	
	public int getId(){
    	return this.id;
    }
    
    public void setId( int id){
    	this.id = id;
    }
	
	public TicketType getTicketType(){
		return this.ticket_type;
	}
	
	public int getEvent_Id(){
		return this.event_id;
	}
	
	public int getAmount(){
		return this.amount;
	}
	
	public int getAmount_Available(){
		return this.amount_available;
	}
	
	public void setTicketType( TicketType tt ){
		this.ticket_type = tt;
	}
	
	public void setEvent_Id( int id ){
		this.event_id = id;
	}
	
	public void setAmount( int am ){
		this.amount = am;
	}
	
	public void setAmount_Available( int am ){
		this.amount_available = am;
	}
	
	@Override
	public String toString(){
		return ticket_type.getName() + ": " + amount_available + "/" + amount + " available";
	}
}
