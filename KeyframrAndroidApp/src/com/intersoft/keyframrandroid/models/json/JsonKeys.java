/*-------------------------------------------------------------------------*/ 
/* Program Name : JsonKeys.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments : Class to store the json keys. These keys will be used when making requests to the server. 
/*-------------------------------------------------------------------------*/
package com.intersoft.keyframrandroid.models.json;

public final class JsonKeys {

	// Log In Json Keys
	public static final String loginUser = "user";
	public static final String LoginName = "name";
	public static final String loginPassword = "password";
	
	// Ticket Json Keys
	public static final String ticketTypeId = "ticket_type_id";
	public static final String ticket = "ticket";
	public static final String keyLocation = "key_location";
	public static final String eventConstraintId = "event_constraint_id";
	public static final String ticketStatus = "status";
	
	//Invoice Json Keys
	public static final String amount = "amount";
	public static final String invoice = "invoice";
	// Customer Json Keys
	public static final String lastName = "lastname";
	public static final String customer = "customer";
	public static final String firstName = "firstname";
	public static final String phoneNumber = "phone_number";
	
	// Vehicle Json Keys
	public static final String vehMake = "make";
	public static final String vehModel = "model";
	public static final String vehColor = "color";
	public static final String vehicle = "vehicle";
	public static final String vehStatus = "status";
	public static final String vehicleId = "vehicle_id";
	public static final String vehLpNumber = "lpnumber";
	public static final String vehLpLocation = "lplocation";
	
	
	private JsonKeys(){
	}
}
