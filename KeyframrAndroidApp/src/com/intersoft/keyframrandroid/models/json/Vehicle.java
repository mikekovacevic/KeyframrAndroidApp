/*-------------------------------------------------------------------------*/ 
/* Program Name : Vehicle.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments : POJO to store JSON data from server. 
/*-------------------------------------------------------------------------*/
package com.intersoft.keyframrandroid.models.json;

import com.google.api.client.util.Key;

public class Vehicle {
	@Key private int id;
    @Key private String make;
    @Key private String model;
    @Key private String color;
    @Key private String lplocation;
    @Key private String lpnumber;
    @Key private String status;

    public String getMake() {
        return this.make;
    }

    public void setMake( String make ) {
        this.make = make;
    }
    
    public String getModel() {
        return this.model;
    }

    public void setModel( String model ) {
        this.model = model;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor( String color ) {
        this.color = color;
    }

    public String getLplocation() {
        return this.lplocation;
    }

    public void setLplocation( String location ) {
        this.lplocation = location;
    }
    
    public String getLpnumber() {
        return this.lpnumber;
    }

    public void setLpnumber( String number ) {
        this.lpnumber = number;
    }
    
    public String getStatus() {
        return this.status;
    }

    public void setStatus( String stat ) {
        this.status = stat;
    }
    
    public int getId(){
    	return this.id;
    }
    
    public void setId( int id){
    	this.id = id;
    }

    @Override
    public String toString() {
        return  lpnumber + " " + lplocation;
    }

}
