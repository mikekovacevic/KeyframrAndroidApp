/*-------------------------------------------------------------------------*/ 
/* Program Name : Ticket.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments : POJO to store JSON data from server. 
/*-------------------------------------------------------------------------*/
package com.intersoft.keyframrandroid.models.json;

import com.google.api.client.util.Key;

public class Ticket {
	@Key private int id;
	@Key private TicketType ticket_type;
    @Key private int vehicleId;
    @Key private String parent;
    @Key private int parent_id;
    @Key private float price;
    
    public float getPrice(){
    	return this.price;
    }
    
    public void setPrice( float p ){
    	this.price = p;
    }
    public TicketType getTicketType(){
		return this.ticket_type;
	}

    public void setTicketType( TicketType tt ){
		this.ticket_type = tt;
	}

    public int getVehicleId() {
        return this.vehicleId;
    }

    public void setVehicleId( int id ) {
        this.vehicleId = id;
    }
    
    public int getId(){
    	return this.id;
    }
    
    public void setId( int id){
    	this.id = id;
    }
    
    public String getParent(){
    	return this.parent;
    }
    
    public void setParent( String p ){
    	this.parent = p;
    }
    
    public int getParentId(){
    	return this.parent_id;
    }
    
    public void setParentId( int pid){
    	this.parent_id = pid;
    }
    
    @Override
    public String toString() {
        return " Ticket [id=" + id + ", ticket type =" + ticket_type.getName() + ", vehicle Id=" + vehicleId;
    }

}