/*-------------------------------------------------------------------------*/ 
/* Program Name : Customer.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments : POJO to store JSON data from server. 
/*-------------------------------------------------------------------------*/

package com.intersoft.keyframrandroid.models.json;

import com.google.api.client.util.Key;

public class Customer {
	@Key ("id") private int id;
    @Key ("firstname") private String firstname;
    @Key ("lastname") private String lastname;
    @Key ("phone_number") private String phone_number;

    public String getFirstName() {
        return this.firstname;
    }

    public void setFirstName( String fn ) {
        this.firstname = fn;
    }

    public String getLastName() {
        return this.lastname;
    }

    public void setLastName( String ln ) {
        this.lastname = ln;
    }

    public String getPhone_Number() {
        return this.phone_number;
    }

    public void setPhone_Number( String pn ) {
        this.phone_number = pn;
    }
    
    public int getId(){
    	return this.id;
    }
    
    public void setId( int id){
    	this.id = id;
    }

    @Override
    public String toString() {
        return lastname + " " + phone_number;
    }
    
    @Override
    public boolean equals(Object that){
    	if(that == null)
    		return false;
    	if(that.getClass() != this.getClass())
    		return false;
    	Customer thatCus = (Customer)that;
    	
    	if(!this.firstname.equals(thatCus.getFirstName()))
    		return false;
    	if(!this.lastname.equals(thatCus.getLastName()))
    		return false;
    	if(!this.phone_number.equals(thatCus.getPhone_Number()))
    		return false;
    	
    	return true;
    }

}