/*-------------------------------------------------------------------------*/ 
/* Program Name : TicketType.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments : POJO to store JSON data from server. 
/*-------------------------------------------------------------------------*/
package com.intersoft.keyframrandroid.models.json;

import com.google.api.client.util.Key;

public class TicketType {
	@Key private int id;
	@Key private String name;
	@Key private float price;
	
	public int getId(){
    	return this.id;
    }
    
    public void setId( int id){
    	this.id = id;
    }
    
    public String getName() {
        return this.name;
    }

    public void setName( String n ) {
        this.name = n;
    }
    
    public float getPrice(){
    	return this.price;
    }
    
    public void setPrice(float p){
    	this.price = p;
    }
    
    @Override
    public String toString(){
    	return name + ": " + price + " dhs";
    }
    
}
