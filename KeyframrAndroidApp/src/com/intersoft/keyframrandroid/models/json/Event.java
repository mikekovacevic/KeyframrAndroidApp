/*-------------------------------------------------------------------------*/ 
/* Program Name : Event.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments : POJO to store JSON data from server. 
/*-------------------------------------------------------------------------*/
package com.intersoft.keyframrandroid.models.json;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.api.client.util.Key;

public class Event {
	@Key ("id") private int id;
	@Key ("customer_id") private int customer_id;
	@Key ("name") private String name;
	@Key ("start_date") private String start_date;
	@Key ("end_date") private String end_date;
	@Key ("event_constraints") private EventConstraint[] event_constraints;
	
    public int getId(){
    	return this.id;
    }
    
    public void setId( int id){
    	this.id = id;
    }
    public int getCustomer_Id(){
    	return this.customer_id;
    }
    
    public void setCustomer_Id( int id){
    	this.customer_id = id;
    }
    
    public String getName(){
    	return this.name;
    }
    
    public void setName(String n){
    	this.name = n;
    }
    
    public EventConstraint[] getEvent_Constraints(){
    	return this.event_constraints;
    }
    
    public void setEvent_Constraints( EventConstraint[] ec){
    	this.event_constraints = ec;
    }
    
    public String getStart_Date(){
    	return this.start_date;
    }
    
    public void setStart_Date(String sd){
    	this.start_date = sd;
    }
    
    public String getEnd_Date(){
    	return this.end_date;
    }
    
    public void setEnd_Date(String ed){
    	this.end_date = ed;
    }
    
    public Date getStartDate(){
    	try {
			return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(this.start_date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
    }
    
    public Date getEndDate(){
    	try {
    		if(!this.end_date.isEmpty())
    			return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(this.end_date);
    		else
    			return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse("9999-12-31T23:59:59.999Z");
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		} 
    }
    @Override 
    public String toString(){
    	return name;
    }
}
