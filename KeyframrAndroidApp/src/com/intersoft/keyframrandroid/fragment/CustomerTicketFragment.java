/*-------------------------------------------------------------------------*/ 
/* Program Name : CustomerTicketFragment.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments : Customer Ticket Fragment. Handles printing of a ticket for a customer. 
/*-------------------------------------------------------------------------*/

package com.intersoft.keyframrandroid.fragment;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import roboguice.inject.InjectResource;
import roboguice.inject.InjectView;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.intersoft.keyframrandroid.R;
import com.intersoft.keyframrandroid.models.json.Customer;
import com.intersoft.keyframrandroid.models.json.Ticket;
import com.intersoft.keyframrandroid.models.json.TicketType;
import com.intersoft.keyframrandroid.requests.CustomerCreateRequest;
import com.intersoft.keyframrandroid.requests.CustomerTicketCreateRequest;
import com.intersoft.keyframrandroid.requests.CustomersGetRequest;
import com.intersoft.keyframrandroid.requests.TicketsTypeGetRequest;
import com.intersoft.keyframrandroid.utils.ExpandAnimation;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

public class CustomerTicketFragment extends TicketFragment {

	private @InjectView(R.id.print_button) Button printButton;
	private @InjectView(R.id.anonButton) ToggleButton anonButton;
	private @InjectView(R.id.customerInfo) LinearLayout cusInfo;
	private @InjectView(R.id.ticketType) Spinner ticketTypeView;
	private @InjectView(R.id.firstName) EditText firstNameView;
	private @InjectView(R.id.lastName) EditText lastNameView;
	private @InjectView(R.id.phoneNo) EditText phoneNoView;
	private @InjectView(R.id.customers) ListView customersList;
	private @InjectResource(R.string.error_field_required) String emptyFieldError;
	private String cusFirstName;
	private String cusLastName;
	private String cusPhoneNo;
	private TicketType ticketType;
	private int customerId;
	private int ticketId;
	private List<TicketType> ticketTypeList = new ArrayList<TicketType>();
	private Customer cus;
    private ArrayAdapter<TicketType> ticketTypeAdapter;
    private ArrayAdapter< Customer > customerAdapter;
    private Customer[] customers;
    
	public static CustomerTicketFragment newInstance() {
		CustomerTicketFragment fragment = new CustomerTicketFragment();
		return fragment;
	}

	public CustomerTicketFragment() {
		// Required empty public constructor
	}


	@Override
	public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        //initialize spinner for ticket types
        initTicketSpinner();
        
        //setup button on click listeners
        printButton.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(checkPrinter())
					setupPrintData();
				
			}
        });

        lastNameView.setOnFocusChangeListener(new OnFocusChangeListener(){

			@Override
			public void onFocusChange(View view, boolean hasFocus) {
				ExpandAnimation expandAni = new ExpandAnimation(customersList, 250);
				customersList.startAnimation(expandAni);
			}
        });

        lastNameView.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text, filter out matching results
                customerAdapter.getFilter().filter(cs);
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) { }
            @Override
            public void afterTextChanged(Editable arg0) {}
        });
        
        
        customersList.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				
				cus = (Customer)parent.getAdapter().getItem(position);
				firstNameView.setText(cus.getFirstName());
				lastNameView.setText(cus.getLastName());
				phoneNoView.setText(cus.getPhone_Number());
				customerId = cus.getId();
				lastNameView.clearFocus();
			}
        });
        
        anonButton.setChecked(true);
        anonButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				ExpandAnimation expandAni = new ExpandAnimation(cusInfo, 500);
	        	cusInfo.startAnimation(expandAni);
			}
		});
        
	}
	// Make sure data is valid for printing (i.e. customer's last name is not empty, etc...). Create a request to server
	// to create the ticket.
	private void setupPrintData(){
		boolean cancel = false;
		View focusView = null;
		if(anonButton.isChecked()){
			// Anonymous Customer
			// Call ticket post request, using a customer id of one. this customer is an anonymous customer always
			customerId = 1;
			getKeyframrActivity().pd.show();
			CustomerTicketCreateRequest request = new CustomerTicketCreateRequest(URL, AuthToken, ticketType.getId(), customerId);
			getSpiceManager().execute(request, new CustomerTicketCreatedListener());
		}
		else{
			// Make sure required fields are not empty
			phoneNoView.setError(null);
			lastNameView.setError(null);
			cusFirstName = firstNameView.getText().toString();
			cusLastName = lastNameView.getText().toString();
			cusPhoneNo= phoneNoView.getText().toString();
			
			Customer temp = new Customer();
			temp.setFirstName(cusFirstName);
			temp.setLastName(cusLastName);
			temp.setPhone_Number(cusPhoneNo);
			
			customerId = isExistingCustomer(temp);
			
			// Check for a valid last name.
			if (TextUtils.isEmpty(cusLastName)) {
				lastNameView.setError(emptyFieldError);
				focusView = lastNameView;
				cancel = true;
			} 
			else if (cusLastName.length() < 3){
				lastNameView.setError("Last name must be at least 3 chars");
				focusView = lastNameView;
				cancel = true;
			}
			if (TextUtils.isEmpty(cusPhoneNo)){
				phoneNoView.setError(emptyFieldError);
				focusView = phoneNoView;
				cancel = true;
			}
			else if (cusPhoneNo.length() < 7){
				phoneNoView.setError("Phone number must be at least 7 digits");
				focusView = phoneNoView;
				cancel = true;
			}
			else if(customerId < 0 && !isUniquePhoneNo(cusPhoneNo)){
				phoneNoView.setError("Phone number must be unique");
				focusView = phoneNoView;
				cancel = true;
			}
			if (cancel) {
				// There was an error; don't attempt ticket creation and focus the first
				// form field with an error.
				focusView.requestFocus();
			}
			else{
				getKeyframrActivity().pd.show();
				
				// submit customer information first, then submit ticket information with the returned customer id
				if(customerId < 0){
					CustomerCreateRequest request = new CustomerCreateRequest(URL, AuthToken, cusFirstName, cusLastName, cusPhoneNo);
					getSpiceManager().execute(request, new CustomerCreatedListener());
				}
				else{
					CustomerTicketCreateRequest request = new CustomerTicketCreateRequest(URL, AuthToken, ticketType.getId(), customerId);
					getSpiceManager().execute(request, new CustomerTicketCreatedListener());
				}
			}
			
		}

	}
	// returns id of customer if it already exists, otherwise it returns -1
	private int isExistingCustomer(Customer cus){
		for(int i = 0; i < customers.length; i++){
			if(customers[i].equals(cus))
				return customers[i].getId();
		}
		return -1;
	}
	// Checks if phone number entered is unique or not
	private boolean isUniquePhoneNo(String phoneNo){
		for(int i = 0; i < customers.length; i++){
			if(customers[i].getPhone_Number().equals(phoneNo))
				return false;
		}
		return true;
	}
	// Initialize ticket type spinner. 
	private void initTicketSpinner(){
	
        ticketTypeAdapter = new ArrayAdapter<TicketType>(getActivity(), android.R.layout.simple_spinner_item, ticketTypeList);
        ticketTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ticketTypeView.setAdapter(ticketTypeAdapter);

        ticketTypeView.setOnItemSelectedListener(new OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
        		ticketTypeView.setSelection(arg2, true);	
				ticketType = ticketTypeAdapter.getItem(arg2);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
        });
        // Add ticket types to the list, then notify the adapter. Notifying the adapter will force the spinner to update with the new values
        ticketTypeAdapter.notifyDataSetChanged();
        ticketTypeView.setSelection(0, true);
	}
	
	public void clearEditViews(){
		firstNameView.setText("");
		lastNameView.setText("");
		phoneNoView.setText("");
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_customer_ticket, container,
				false);
	}
	
	@Override
	public void onResume(){
		super .onResume();
		if(ticketTypeList.isEmpty()){
	        //request ticket types and populate spinner
	        TicketsTypeGetRequest request = new TicketsTypeGetRequest(URL, AuthToken);
	        getSpiceManager().execute(request, "tickettype", 3*DurationInMillis.ONE_SECOND, new TicketsTypeRequestListener());
  
		}
		// get a list of all customers
        CustomersGetRequest cusRequest = new CustomersGetRequest(URL, AuthToken);
        getSpiceManager().execute(cusRequest, new CustomersGetListener());
	}
	public final class TicketsTypeRequestListener implements RequestListener< TicketType[] > {
		
	    @Override
	    public void onRequestFailure( SpiceException spiceException ) {
	        Toast.makeText( getActivity(), "Could not retrieve ticket types.", Toast.LENGTH_SHORT ).show();
	    }
	
	    @Override
	    public void onRequestSuccess( final TicketType[] result ) {
	    	for(int i=0; i < result.length; i++){
				ticketTypeList.add(result[i]);
	    	}

	    	ticketTypeAdapter.notifyDataSetChanged();
	    }
	}
	
	public final class CustomerCreatedListener implements RequestListener< Customer >{

		@Override
		public void onRequestFailure(SpiceException arg0) {
			Toast.makeText(getActivity(), arg0.getMessage(), Toast.LENGTH_SHORT).show();
			getKeyframrActivity().pd.dismiss();
		}

		@Override
		public void onRequestSuccess(Customer arg0) {
			customerId = arg0.getId();
			// customer is created, let's create the ticket
			CustomerTicketCreateRequest request = new CustomerTicketCreateRequest(URL, AuthToken, ticketType.getId(), customerId);
			getSpiceManager().execute(request, new CustomerTicketCreatedListener());
		}
		
	}
	
	public final class CustomerTicketCreatedListener implements RequestListener< Ticket >{

		@Override
		public void onRequestFailure(SpiceException arg0) {
			
			Toast.makeText(getActivity(), arg0.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
			getKeyframrActivity().pd.dismiss();
		}
		// Customer Ticket is created successfully. Let's print out the ticket for the customer and notify the parent activity
		// of our completion.
		@Override
		public void onRequestSuccess(Ticket arg0) {
			clearEditViews();
			ticketId = arg0.getId();
			customerId = arg0.getParentId();
			
			printTicket(ticketId, customerId, -1, -1);
			mListener.onComplete(ticketId, customerId, -1);
			getKeyframrActivity().pd.dismiss();
		}
		
	}
	// Listener for the customers get request. On success, we will have all existing customers.
	public final class CustomersGetListener implements RequestListener< Customer[] >{

		@Override
		public void onRequestFailure(SpiceException arg0) {
			Toast.makeText(getActivity(), arg0.getMessage(), Toast.LENGTH_SHORT).show();
		}
		// Sort customers by alphabetical order (descending).
		@Override
		public void onRequestSuccess(Customer[] cus) {
			customers = cus;
			customerAdapter = new ArrayAdapter< Customer >(getActivity()
					, android.R.layout.simple_spinner_item, cus);
			customerAdapter.sort(new Comparator<Customer>() {
			    public int compare(Customer object1, Customer object2) {
			    	return object1.getLastName().compareToIgnoreCase(object2.getLastName());
			    };
			});
			customersList.setAdapter(customerAdapter);
		}
		
	}
}
