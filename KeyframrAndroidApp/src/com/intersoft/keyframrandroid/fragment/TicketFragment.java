/*-------------------------------------------------------------------------*/ 
/* Program Name : TicketFragment.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments : Ticket Fragment. Contains some common functions that are used between the ticket fragments, such as 
/* the onComplete interface and the printTicket function. 
/*-------------------------------------------------------------------------*/

package com.intersoft.keyframrandroid.fragment;

import it.custom.printer.api.android.CustomException;
import it.custom.printer.api.android.CustomPrinter;
import it.custom.printer.api.android.PrinterFont;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.Toast;

import com.codepath.libraries.androidviewhelpers.SimpleAlertDialog;
import com.intersoft.keyframrandroid.activity.OpenTicketActivity;
import com.intersoft.keyframrandroid.activity.SettingsActivity;
import com.intersoft.keyframrandroid.base.BaseKeyframrActivity;
import com.intersoft.keyframrandroid.base.BaseKeyframrFragment;
import com.intersoft.keyframrandroid.utils.Contents;
import com.intersoft.keyframrandroid.utils.QRContents;
import com.intersoft.keyframrandroid.utils.TicketQRWriter;

public class TicketFragment extends BaseKeyframrFragment {
	protected OnCompleteListener mListener;
	protected static final String CUSTOMER_FLAG = "customers";
	protected static final String EVENT_FLAG = "events";
	
	public boolean checkPrinter(){
		SimpleAlertDialog sa = SimpleAlertDialog.build(getActivity(), "Printer Disconnected. Please Turn it on and try again.", new SimpleAlertDialog.SimpleAlertListener(){

			@Override
			public void onPositive() {
				return;
			}

			@Override
			public void onNegative() {
			}
		});
		
		sa.setAlertButtonText("Ok", null);
		
		try{
			getKeyframrActivity().reconnectPrinter();
			CustomPrinter prnDevice = BaseKeyframrActivity.getPrinter();
			prnDevice.isPrinterOnline();
		}
		catch(CustomException e){
			sa.show();
			e.printStackTrace();
			return false;
		}
		catch(NullPointerException e){
			sa.show();
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static void printTicket(int ticketId, int cusId, int eveId, int vehId)  
	{  
		//Create QR code contents
		String qrContents = new QRContents(ticketId, cusId, eveId, vehId).toString();
		String ticketHeader;
		if(qrContents.contains(Contents.Header.CUSTOMER) || qrContents.contains(Contents.Header.EVENT))
			ticketHeader = "Customer Ticket";
		else if(qrContents.contains(Contents.Header.VEHICLE))
			ticketHeader = "Vehicle Ticket";
		else
			ticketHeader = "Keys Ticket";
		Log.v("contents", qrContents);
		//Create the Bitmap
		Bitmap image = new TicketQRWriter(qrContents,300).encodeBitmap();
		CustomPrinter prnDevice = BaseKeyframrActivity.getPrinter();

		synchronized ("lockAccess") 
		{
			//***************************************************************************
			// PRINT PICTURE
			//***************************************************************************
			PrinterFont fntPrinterBold2X = new PrinterFont();
	    	try
	        {
	    		// Setup printer font (Bold font, double size of normal text)
	    		//Fill class: BOLD size 2X
		    	fntPrinterBold2X.setCharHeight(PrinterFont.FONT_SIZE_X2);					//Height x2
		    	fntPrinterBold2X.setCharWidth(PrinterFont.FONT_SIZE_X2);					//Width x2
		    	fntPrinterBold2X.setEmphasized(true);										//Bold
		    	fntPrinterBold2X.setItalic(false);											//No Italic
		    	fntPrinterBold2X.setUnderline(false);										//No Underline
		    	fntPrinterBold2X.setJustification(PrinterFont.FONT_JUSTIFICATION_CENTER);	//Center	    	
		    	fntPrinterBold2X.setInternationalCharSet(PrinterFont.FONT_CS_DEFAULT);	
		    	
	    		prnDevice.printTextLF(ticketHeader, fntPrinterBold2X);
	    		
	    		//Print (Left Align and Fit to printer width)
	    		prnDevice.printImage(image,CustomPrinter.IMAGE_ALIGN_TO_LEFT, CustomPrinter.IMAGE_SCALE_TO_FIT, 0);
	    		prnDevice.printTextLF("Ticket ID: " + ticketId);
	        }
	    	catch(CustomException e )
	        {            	
	        	//Show Error
	    		e.printStackTrace();       		
	        }
			catch(Exception e )
	        {
				e.printStackTrace();
	        }

			//***************************************************************************
			// FEEDS and CUT
			//***************************************************************************
			
			try
	        {
				//Feeds (3)
				prnDevice.feed(3);
	    		//Cut (Total)
	    		prnDevice.cut(CustomPrinter.CUT_TOTAL);	    		
	        }
	    	catch(CustomException e )
	        {    
	    		//Only if isn't unsupported
	    		if (e.GetErrorCode() != CustomException.ERR_UNSUPPORTEDFUNCTION)
	    		{
	    			//Show Error
	    			e.printStackTrace();
	    		}
	        }
			catch(Exception e )
	        {
				e.printStackTrace();;
	        }
			
			//***************************************************************************
			// PRESENT
			//***************************************************************************
			
			try
	        {				
	    		//Present (40mm)
	    		prnDevice.present(40);
	        }
	    	catch(CustomException e )
	        {    
	    		//Only if isn't unsupported
	    		if (e.GetErrorCode() != CustomException.ERR_UNSUPPORTEDFUNCTION)
	    		{
	    			//Show Error
	    			e.printStackTrace();
	    		}
	        }
		}
	} 
	
	public static interface OnCompleteListener {
	    public abstract void onComplete(int ticket_id, int cusid, int eventid);
	}

	// make sure the Activity implemented it
	@Override
	public void onAttach(Activity activity) {
		super .onAttach(activity);
	    try {
	        this.mListener = (OnCompleteListener)activity;
	    }
	    catch (final ClassCastException e) {
	        throw new ClassCastException(activity.toString() + " must implement OnCompleteListener");
	    }
	}
}
