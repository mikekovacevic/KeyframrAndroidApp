/*-------------------------------------------------------------------------*/ 
/* Program Name : PairedDevicesFragment.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments : Paired Devices Fragment. Dialog Fragment that shows a list view of all paired bluetooth devices. 
/*-------------------------------------------------------------------------*/

package com.intersoft.keyframrandroid.fragment;

import it.custom.printer.api.android.CustomAndroidAPI;
import it.custom.printer.api.android.CustomException;

import java.util.ArrayList;
import java.util.Arrays;

import roboguice.RoboGuice;
import roboguice.inject.InjectView;
import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockDialogFragment;
import com.intersoft.keyframrandroid.R;
import com.intersoft.keyframrandroid.base.BaseKeyframrActivity;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass. Use the
 * {@link PairedDevicesFragment#newInstance} factory method to create an
 * instance of this fragment.
 * 
 */
public class PairedDevicesFragment extends RoboSherlockDialogFragment {
	private static final String ARG_PREVDEVICE = "prevdevice";
	private @InjectView(R.id.listPairedDevs) ListView pairedList;
	private BluetoothDevice[] btDeviceList;
	private OnCompleteListener mListener;
	static ArrayAdapter<String> listAdapter;  
	
	static int deviceSelected = -1;
	private String lastDevice;


	public static PairedDevicesFragment newInstance(String prevDevice) {
		PairedDevicesFragment fragment = new PairedDevicesFragment();
		Bundle args = new Bundle();
		args.putString(ARG_PREVDEVICE, prevDevice);
		fragment.setArguments(args);
		return fragment;
	}

	public PairedDevicesFragment() {
		// Required empty public constructor
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        RoboGuice.getInjector(getActivity()).injectViewMembers(this);
		getDialog().setTitle("Paired Devices");
		lastDevice = getArguments().getString(ARG_PREVDEVICE);
		// Show soft keyboard automatically
		initListView();
	}

	private void initListView(){
		try
        {
        	//Get the list of devices 
			btDeviceList = CustomAndroidAPI.EnumBluetoothDevices();
			
        	if ((btDeviceList == null) || (btDeviceList.length == 0))
        	{
        		//Show Error
        		((BaseKeyframrActivity)getActivity()).showAlertMsg("Error...", "No Devices Connected...");                		
        		return;
        	}                               	
        }
		catch(CustomException e )
        {
        	
        	//Show Error
			((BaseKeyframrActivity)getActivity()).showAlertMsg("Error...", e.getMessage());
    		return;
        }
        catch(Exception e )
        {
        	
        	//Show Error
        	((BaseKeyframrActivity)getActivity()).showAlertMsg("Error...", "Enum devices error...");
    		return;
        }
        String[] strDevices = new String[btDeviceList.length];
        for (int i=0;i<btDeviceList.length;i++)
        {
        	strDevices[i] = btDeviceList[i].getName();
        	if (strDevices[i].equals(lastDevice)){
        		deviceSelected = i;
        	}
        }
        
        ArrayList<String> devicesList = new ArrayList<String>();  
        devicesList.addAll( Arrays.asList(strDevices) );  
          
        // Create ArrayAdapter using the list.  
        listAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_single_choice, devicesList);  	              	              
          
        // Set the ArrayAdapter as the ListView's adapter.  
        pairedList.setAdapter( listAdapter );          
        
        pairedList.setItemsCanFocus(false);        
        pairedList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);   
        pairedList.setItemChecked(deviceSelected, true);
        pairedList.setOnItemClickListener(new OnItemClickListener() {
        	@Override
        	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) 
        	{				
        		//Save position Value
        		mListener.onComplete(btDeviceList[arg2]);	
        		getDialog().dismiss();
			}
		});
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_paired_devices_dialog, container, false);
	}

	public static interface OnCompleteListener {
	    public abstract void onComplete(BluetoothDevice selected);
	}

	// make sure the Activity implemented it
	@Override
	public void onAttach(Activity activity) {
		super .onAttach(activity);
	    try {
	        this.mListener = (OnCompleteListener)activity;
	    }
	    catch (final ClassCastException e) {
	        throw new ClassCastException(activity.toString() + " must implement OnCompleteListener");
	    }
	}
}
