/*-------------------------------------------------------------------------*/ 
/* Program Name : VehicleTicketFragment.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments : Vehicle Ticket Fragment. Handles printing of a ticket for a vehicle. A lot of the functions of the 
 * Vehicle Ticket Fragment are very similar to those of the customer ticket fragment.
 */
/*-------------------------------------------------------------------------*/


package com.intersoft.keyframrandroid.fragment;

import roboguice.inject.InjectResource;
import roboguice.inject.InjectView;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.intersoft.keyframrandroid.R;
import com.intersoft.keyframrandroid.base.BaseKeyframrDialogFragment;
import com.intersoft.keyframrandroid.models.json.Ticket;
import com.intersoft.keyframrandroid.models.json.Vehicle;
import com.intersoft.keyframrandroid.requests.TicketUpdateRequest;
import com.intersoft.keyframrandroid.requests.TicketUpdateRequestBuilder;
import com.intersoft.keyframrandroid.requests.VehicleCreateRequest;
import com.intersoft.keyframrandroid.requests.VehiclesGetRequest;
import com.intersoft.keyframrandroid.utils.ExpandAnimation;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

public class VehicleTicketFragment extends BaseKeyframrDialogFragment {

	private static final String ARG_TICKETID = "ticket_id";
	private static final String ARG_CUSTOMERID = "customer_id";
	private static final String ARG_EVENTID = "event_id";
	private @InjectView(R.id.print_button) Button printButton;
	private @InjectView(R.id.anonVeh) ToggleButton anonButton;
	private @InjectView(R.id.vehicleInfo) LinearLayout vehInfo;
	private @InjectView(R.id.lpcity) EditText lpCityView;
	private @InjectView(R.id.lpnumber) EditText lpNumberView;
	private @InjectView(R.id.make) EditText makeView;
	private @InjectView(R.id.model) EditText modelView;
	private @InjectView(R.id.color) EditText colorView;
	private @InjectView(R.id.vehicles) ListView vehicleList;
	private @InjectResource(R.string.error_field_required) String emptyFieldError;
	private String lpCity;
	private String lpNumber;
	private String make;
	private String model;
	private String color;
	private int vehicleId;
	private int ticketId;
	private int eventId;
	private int customerId;
	private Vehicle veh;
    private ArrayAdapter< Vehicle > vehicleAdapter;
    private Vehicle[] vehicles;
    
	public static VehicleTicketFragment newInstance(int tId, int cusId, int eventId) {
		VehicleTicketFragment fragment = new VehicleTicketFragment();
		Bundle args = new Bundle();
		
		args.putInt(ARG_TICKETID, tId);
		args.putInt(ARG_CUSTOMERID, cusId);
		args.putInt(ARG_EVENTID, eventId);
		fragment.setArguments(args);
		
		
		return fragment;
	}

	public VehicleTicketFragment() {
		// Required empty public constructor
	}


	@Override
	public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        
        ticketId = getArguments().getInt(ARG_TICKETID);
        customerId = getArguments().getInt(ARG_CUSTOMERID);
        eventId = getArguments().getInt(ARG_EVENTID);
        
        //request vehicles index from server
        VehiclesGetRequest vehRequest = new VehiclesGetRequest(URL, AuthToken);
        getSpiceManager().execute(vehRequest, new VehiclesGetListener());
        
        //setup button on click listeners
        printButton.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				setupPrintData();
				
			}
        });

        lpNumberView.setOnFocusChangeListener(new OnFocusChangeListener(){

			@Override
			public void onFocusChange(View view, boolean hasFocus) {
				ExpandAnimation expandAni = new ExpandAnimation(vehicleList, 250);
				vehicleList.startAnimation(expandAni);
			}
        });

        lpNumberView.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
            	vehicleAdapter.getFilter().filter(cs);
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) { }
            @Override
            public void afterTextChanged(Editable arg0) {}
        });
        
        
        vehicleList.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				
				veh = (Vehicle)parent.getAdapter().getItem(position);
				lpCityView.setText(veh.getLplocation());
				lpNumberView.setText(veh.getLpnumber());
				makeView.setText(veh.getMake());
				modelView.setText(veh.getModel());
				colorView.setText(veh.getColor());
				vehicleId = veh.getId();
				lpNumberView.clearFocus();
			}
        });
        
        anonButton.setChecked(true);
        anonButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				ExpandAnimation expandAni = new ExpandAnimation(vehInfo, 75);
	        	vehInfo.startAnimation(expandAni);
			}
		});
        
	}
	private void setupPrintData(){
		boolean cancel = false;
		View focusView = null;
		if(anonButton.isChecked()){
			// Anonymous Vehicle
			// Do Not update Ticket, just print out the vehicle ticket
			printVehicleTickets(ticketId, 0);
		}
		else{
			// Make sure required fields are not empty
			lpNumberView.setError(null);
			lpCity = lpCityView.getText().toString();
			lpNumber = lpNumberView.getText().toString();
			make = makeView.getText().toString();
			model = modelView.getText().toString();
			color = colorView.getText().toString();
			
			// Check for a valid password.
			if (TextUtils.isEmpty(lpNumber)) {
				lpNumberView.setError(emptyFieldError);
				focusView = lpNumberView;
				cancel = true;
			} 
	
			if (cancel) {
				// There was an error; don't attempt login and focus the first
				// form field with an error.
				focusView.requestFocus();
			}
			else{
				getKeyframrActivity().pd.show();
				vehicleId = isUniqueLPNumber(lpNumber);
				// if vehicle id is negative, we have to create a new vehicle. otherwise, use the retrieved vehicle id to print a new ticket
				if(vehicleId < 0){
					VehicleCreateRequest request = new VehicleCreateRequest(URL, AuthToken, lpNumber, lpCity, make, model, color);
					getSpiceManager().execute(request, new VehicleCreatedListener());
				}
				else{
					updateTicketAndPrint();
				}
	
			}
		}	

	}
	private void updateTicketAndPrint(){
		TicketUpdateRequestBuilder builder = new TicketUpdateRequestBuilder()
			.setCustomerId(customerId)
			.setEventId(eventId)
			.setVehicleId(vehicleId);
		TicketUpdateRequest request = builder.build(URL, AuthToken, ticketId);
		getSpiceManager().execute(request, new TicketUpdatedListener());
	}
	
	public final class TicketUpdatedListener implements RequestListener< Ticket >{
		@Override
		public void onRequestFailure(SpiceException arg0) {
			Toast.makeText(getActivity(), arg0.getMessage(), Toast.LENGTH_SHORT).show();
			getKeyframrActivity().pd.dismiss();
		}
		// Ticket has been updated successfully, print it out!
		@Override
		public void onRequestSuccess(Ticket arg0) {
			getKeyframrActivity().pd.dismiss();
			printVehicleTickets(ticketId, vehicleId);
			getDialog().dismiss();
		}
		
	}
	
	private void printVehicleTickets(int ticket_Id, int vehId){
		// Vehicle Ticket
		TicketFragment.printTicket(ticket_Id, -1, -1, vehId);
		// Keys Ticket
		TicketFragment.printTicket(ticket_Id, -1, -1, -1);
		
		getDialog().dismiss();
	}
	
	private int isUniqueLPNumber(String lpn){
		for(int i = 0; i < vehicles.length; i++){
			if(vehicles[i].getLpnumber().equals(lpn))
				return vehicles[i].getId();
		}
		return -1;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_vehicle_ticket, container,
				false);
		getDialog().setTitle("Vehicle Tickets");
		setCancelable(false);
		view.setMinimumHeight(100);
		view.setMinimumWidth(50);
		return view;
	}


	
	public final class VehicleCreatedListener implements RequestListener< Vehicle >{

		@Override
		public void onRequestFailure(SpiceException arg0) {
			Toast.makeText(getActivity(), arg0.getMessage(), Toast.LENGTH_SHORT).show();
			getKeyframrActivity().pd.dismiss();
		}

		@Override
		public void onRequestSuccess(Vehicle arg0) {
			vehicleId = arg0.getId();
			// vehicle is created, let's update ticket and print the ticket for the vehicle
			updateTicketAndPrint();
		}
		
	}
	
	public final class VehiclesGetListener implements RequestListener< Vehicle[] >{

		@Override
		public void onRequestFailure(SpiceException arg0) {
			Toast.makeText(getActivity(), arg0.getMessage(), Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onRequestSuccess(Vehicle[] veh) {
			vehicles = veh;
			vehicleAdapter = new ArrayAdapter< Vehicle >(getActivity()
					, android.R.layout.simple_spinner_item, veh);
			vehicleList.setAdapter(vehicleAdapter);
		}
		
	}
	
	
}
