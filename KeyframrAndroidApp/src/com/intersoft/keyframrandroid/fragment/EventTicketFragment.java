/*-------------------------------------------------------------------------*/ 
/* Program Name : EventTicketFragment.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments : Event Ticket Fragment. Handles printing of a ticket for an event. 
/*-------------------------------------------------------------------------*/


package com.intersoft.keyframrandroid.fragment;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import roboguice.inject.InjectView;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.intersoft.keyframrandroid.R;
import com.intersoft.keyframrandroid.models.json.Event;
import com.intersoft.keyframrandroid.models.json.EventConstraint;
import com.intersoft.keyframrandroid.models.json.Ticket;
import com.intersoft.keyframrandroid.requests.EventTicketCreateRequest;
import com.intersoft.keyframrandroid.requests.EventsGetRequest;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass. Activities that
 * contain this fragment must implement the
 * {@link EventTicketFragment.OnFragmentInteractionListener} interface to handle
 * interaction events. Use the {@link EventTicketFragment#newInstance} factory
 * method to create an instance of this fragment.
 * 
 */
public class EventTicketFragment extends TicketFragment {

	private @InjectView(R.id.print_button) Button printButton;

	private @InjectView(R.id.events) ListView eventsList;
	private @InjectView(R.id.eventTicketType) Spinner eveConSpinner;
	
    private ArrayAdapter< Event > eventAdapter;
    private Event selectedEvent;
    private ArrayAdapter<EventConstraint> eventConstraints;
    private int ticketTypeId;
    private EventConstraint eventCon;
	public static EventTicketFragment newInstance(String param1, String param2) {
		EventTicketFragment fragment = new EventTicketFragment();
		return fragment;
	}

	public EventTicketFragment() {
		// Required empty public constructor
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
        printButton.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(checkPrinter())
					setupPrintData();
			}
        });
        eventsList.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				if(eventsList.getItemAtPosition(arg2).getClass() == String.class)
					return;
				selectedEvent = (Event)eventsList.getItemAtPosition(arg2);
		        eventConstraints = new ArrayAdapter<EventConstraint>(getActivity(), 
		        		android.R.layout.simple_spinner_item, selectedEvent.getEvent_Constraints());
		        eventConstraints.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		        eveConSpinner.setAdapter(eventConstraints);
		        eveConSpinner.setOnItemSelectedListener(new OnItemSelectedListener(){

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						eveConSpinner.setSelection(arg2, true);	
						ticketTypeId = eventConstraints.getItem(arg2).getTicketType().getId();
						eventCon = eventConstraints.getItem(arg2);
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
					}
		        });
		        // Add ticket types to the list, then notify the adapter. Notifying the adapter will force the spinner to update with the new values
		        eventConstraints.notifyDataSetChanged();
		        eveConSpinner.setSelection(0, true);
				
			}
        	
        });
	}
	// Make sure an event is selected, then submit an event ticket post request
	public void setupPrintData(){
		int selected = eventsList.getCheckedItemPosition();
		if(selected == ListView.INVALID_POSITION ){
			getKeyframrActivity().showAlertMsg("No Event Selected", "Please select an event");
		}
		else{
			if(eventCon.getAmount_Available() < 1)
				getKeyframrActivity().showAlertMsg("Invalid.", "No " + eventCon.getTicketType().getName() + 
						" Tickets left. Please choose another type of ticket.");
			else{
				getKeyframrActivity().pd.show();
				selectedEvent = (Event)eventsList.getItemAtPosition(selected);
				EventTicketCreateRequest request = new EventTicketCreateRequest(URL, AuthToken, selectedEvent.getId(), ticketTypeId, eventCon.getId());
				getSpiceManager().execute(request, new EventTicketCreatedListener());
			}
		}
	}
	// Get All existing, valid events.
	@Override
	public void onResume(){
		super .onResume();
        EventsGetRequest request = new EventsGetRequest(URL, AuthToken);
        getSpiceManager().execute(request, new EventsGetListener());
    
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
	
	return inflater.inflate(R.layout.fragment_event_ticket, container, false);
	}
	// Setup events list view.
	public final class EventsGetListener implements RequestListener< Event[] >{

		@Override
		public void onRequestFailure(SpiceException arg0) {
			Toast.makeText(getActivity(), arg0.getMessage(), Toast.LENGTH_SHORT).show();
			ArrayAdapter< String > empty = new ArrayAdapter< String > (getActivity()
					, android.R.layout.simple_spinner_item);
			empty.add("No Events Available");
			printButton.setVisibility(View.GONE);
			eventsList.setAdapter(empty);
			eveConSpinner.setVisibility(View.GONE);
		}

		@Override
		public void onRequestSuccess(Event[] eve) {

			if(eve.length < 1){
				ArrayAdapter< String > empty = new ArrayAdapter< String > (getActivity()
						, android.R.layout.simple_spinner_item);
				empty.add("No Events Available");
				eventsList.setAdapter(empty);
				eveConSpinner.setVisibility(View.GONE);
				eventsList.setClickable(false);
			}
			else{
				eveConSpinner.setVisibility(View.VISIBLE);
				eventsList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
				eventsList.setItemsCanFocus(false); 
				eventsList.setClickable(true);
				printButton.setVisibility(View.VISIBLE);
				eventAdapter = new ArrayAdapter< Event >(getActivity()
						, android.R.layout.simple_list_item_single_choice, eve);
				// Sort events by end date. Events with the nearest end dates appear first
				eventAdapter.sort(new Comparator<Event>() {
				    public int compare(Event object1, Event object2) {
				    	return object1.getEndDate().compareTo(object2.getEndDate());
				    };
				});
				eventsList.setAdapter(eventAdapter);
			}
		}
		
	}
	// Event ticket post listener
	public final class EventTicketCreatedListener implements RequestListener< Ticket >{

		@Override
		public void onRequestFailure(SpiceException arg0) {
			
			Toast.makeText(getActivity(), arg0.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
			getKeyframrActivity().pd.dismiss();
		}
		// Successfully created event ticket. Gather information needed, print out the ticket and then notify calling activity
		// of completion.
		@Override
		public void onRequestSuccess(Ticket arg0) {
			int ticketId = arg0.getId();
			int eventId = arg0.getParentId();

			// Refresh Events
	        EventsGetRequest request = new EventsGetRequest(URL, AuthToken);
	        getSpiceManager().execute(request, new EventsGetListener());
	        eveConSpinner.setAdapter(null);
	        // Print ticket...
			printTicket(ticketId, -1, eventId, -1);
			mListener.onComplete(ticketId, -1, eventId);
			getKeyframrActivity().pd.dismiss();
		}
		
	}
}
