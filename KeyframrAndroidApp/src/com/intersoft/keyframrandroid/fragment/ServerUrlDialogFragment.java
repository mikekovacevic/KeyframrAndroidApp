/*-------------------------------------------------------------------------*/ 
/* Program Name : ServerUrlDialogFragment.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments : Prompts user to enter a valid server url. This fragment will also perform a check to see if the server 
 * url is indeed valid. Check is done via a simple ping to the given server url. 
 */
/*-------------------------------------------------------------------------*/

package com.intersoft.keyframrandroid.fragment;

import roboguice.RoboGuice;
import roboguice.inject.InjectView;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.intersoft.keyframrandroid.R;
import com.intersoft.keyframrandroid.requests.ServerPingRequest;
import com.intersoft.keyframrandroid.spiced.SpicedRoboSherlockDialogFragment;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass. Use the
 * {@link ServerUrlDialogFragment#newInstance} factory method to create an
 * instance of this fragment.
 * 
 */
public class ServerUrlDialogFragment extends SpicedRoboSherlockDialogFragment {
	private static final String ARG_CANCELLABLE = "cancellable";

	private @InjectView(R.id.server_url) EditText serverUrlEdit;
	private @InjectView(R.id.server_confirm_button) Button confirmButton;
	private @InjectView(R.id.server_cancel_button) Button cancelButton;
	private String inputUrl;
	private OnCompleteListener mListener;
	private SharedPreferences serverPrefs;

	public static ServerUrlDialogFragment newInstance( boolean cancellable) {
		ServerUrlDialogFragment fragment = new ServerUrlDialogFragment();
		Bundle args = new Bundle();
		
		args.putBoolean(ARG_CANCELLABLE, cancellable);
		fragment.setArguments(args);
		return fragment;
	}

	public ServerUrlDialogFragment() {
		// Required empty public constructor
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        RoboGuice.getInjector(getActivity()).injectViewMembers(this);
        serverPrefs = getActivity().getSharedPreferences("ServerUrl", 0);
		getDialog().setTitle("Server URL");
		shouldCancel(getArguments().getBoolean(ARG_CANCELLABLE));
		// Show soft keyboard automatically
		serverUrlEdit.requestFocus();
		getDialog().getWindow().setSoftInputMode(
				LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		// When confirmed, create a ping request to the entered URL.
		confirmButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View view) {
				inputUrl = serverUrlEdit.getText().toString();
				if(!inputUrl.contains("http:")){
					inputUrl = "http://" + inputUrl;
				}
				ServerPingRequest request = new ServerPingRequest(inputUrl);
				getSpiceManager().execute(request, new ServerPingListener());

			}
		});
	}
	
	private void shouldCancel(boolean cancel){
		setCancelable(cancel);
		if(cancel){
			serverUrlEdit.setText(serverPrefs.getString("url", ""));

			cancelButton.setVisibility(View.VISIBLE);
			cancelButton.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					getDialog().dismiss();
				}
			});
		}
		else{
			cancelButton.setVisibility(View.GONE);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_server_url_dialog, container, false);

		return view;
	}
	public static interface OnCompleteListener {
	    public abstract void onComplete(String url);
	}

	// make sure the Activity implemented it
	@Override
	public void onAttach(Activity activity) {
		super .onAttach(activity);
	    try {
	        this.mListener = (OnCompleteListener)activity;
	    }
	    catch (final ClassCastException e) {
	        throw new ClassCastException(activity.toString() + " must implement OnCompleteListener");
	    }
	}
	// Listener for ping
	public final class ServerPingListener implements RequestListener< String >{
		// On fail, do not save the URL
		@Override
		public void onRequestFailure(SpiceException arg0) {
			Toast.makeText(getActivity(), "Invalid Server URL", Toast.LENGTH_SHORT).show();
		}
		// On success, save the url and notify the calling activity
		@Override
		public void onRequestSuccess(String arg0) {
			SharedPreferences.Editor editor = serverPrefs.edit();
            // save the returned URL into
            // the SharedPreferences
			editor.putString("url", inputUrl);
            editor.commit();
            mListener.onComplete(inputUrl);	
			getDialog().dismiss();
			
		}
		
	}

}
