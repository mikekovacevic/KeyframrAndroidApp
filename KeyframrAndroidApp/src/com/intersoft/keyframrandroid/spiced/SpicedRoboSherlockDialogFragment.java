/*-------------------------------------------------------------------------*/ 
/* Program Name : SpicedRoboSherlockDialogFragment.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments :  Parent of a BaseKeyframrDialogFragment. Sets up the Spice Manager (for networking and http requests) 
/*-------------------------------------------------------------------------*/
package com.intersoft.keyframrandroid.spiced;

import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockDialogFragment;
import com.octo.android.robospice.SpiceManager;

public abstract class SpicedRoboSherlockDialogFragment extends RoboSherlockDialogFragment{
	private SpiceManager spiceManager = new SpiceManager(JsonSpiceService.class);
	@Override
	public void onStart() {
	    super.onStart();
	    spiceManager.start(getActivity());
	}	
	@Override
	public void onStop() {
	    if (spiceManager.isStarted()) {
	        spiceManager.shouldStop();
	    }
	    super.onStop();
	}
	protected SpiceManager getSpiceManager() {
        return spiceManager;
    }
}