/*-------------------------------------------------------------------------*/ 
/* Program Name : JsonSpiceService.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments :  Extension of a Spice Service. Allows for storage of http request results in the cache for 
 * future use. 
/*-------------------------------------------------------------------------*/

package com.intersoft.keyframrandroid.spiced;


import android.app.Application;

import com.octo.android.robospice.GoogleHttpClientSpiceService;
import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.persistence.googlehttpclient.json.Jackson2ObjectPersisterFactory;

public class JsonSpiceService extends GoogleHttpClientSpiceService {

    @Override
    public CacheManager createCacheManager( Application application ) throws CacheCreationException{
        CacheManager cacheManager = new CacheManager();
        Jackson2ObjectPersisterFactory jackson2ObjectPersisterFactory = new Jackson2ObjectPersisterFactory( application );
        cacheManager.addPersister( jackson2ObjectPersisterFactory );
        return cacheManager;
    }

}