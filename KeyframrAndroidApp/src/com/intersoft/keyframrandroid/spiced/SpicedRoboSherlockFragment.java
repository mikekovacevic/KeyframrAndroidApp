/*-------------------------------------------------------------------------*/ 
/* Program Name : SpicedRoboSherlockFragment.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments :  Parent of a BaseKeyframrFragment. Sets up the Spice Manager (for networking and http requests) 
/*-------------------------------------------------------------------------*/
package com.intersoft.keyframrandroid.spiced;

import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockFragment;
import com.octo.android.robospice.SpiceManager;

public abstract class SpicedRoboSherlockFragment extends RoboSherlockFragment{
	private SpiceManager spiceManager = new SpiceManager(JsonSpiceService.class);
	@Override
	public void onStart() {
	    super.onStart();
	    spiceManager.start(getActivity());
	}	
	@Override
	public void onStop() {
	    // Please review https://github.com/octo-online/robospice/issues/96 for the reason of that
	    // ugly if statement.
	    if (spiceManager.isStarted()) {
	        spiceManager.shouldStop();
	    }
	    super.onStop();
	}
	protected SpiceManager getSpiceManager() {
        return spiceManager;
    }
}
