/*-------------------------------------------------------------------------*/ 
/* Program Name : SpicedRoboSherlockFragmentActivity.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments :  Parent of a BaseKeyframrActivity. Sets up the Spice Manager (for networking and http requests) 
/*-------------------------------------------------------------------------*/
package com.intersoft.keyframrandroid.spiced;

import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockFragmentActivity;
import com.octo.android.robospice.SpiceManager;


public abstract class SpicedRoboSherlockFragmentActivity extends RoboSherlockFragmentActivity {
    private SpiceManager spiceManager = new SpiceManager( JsonSpiceService.class );

    @Override
    protected void onStart() {
        spiceManager.start( this );
        super.onStart();
    }
    
    @Override
    protected void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    protected SpiceManager getSpiceManager() {
        return spiceManager;
    }


}