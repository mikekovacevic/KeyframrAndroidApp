/*-------------------------------------------------------------------------*/ 
/* Program Name : TicketUpdateRequest.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments :  Creates a ticket put request to server. 
/*-------------------------------------------------------------------------*/
package com.intersoft.keyframrandroid.requests;

import java.io.IOException;

import org.json.JSONException;

import roboguice.util.temp.Ln;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.GenericData;
import com.intersoft.keyframrandroid.models.json.JsonKeys;
import com.intersoft.keyframrandroid.models.json.Ticket;
import com.intersoft.keyframrandroid.utils.TicketStatus;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

public class TicketUpdateRequest extends GoogleHttpClientSpiceRequest< Ticket > {

    private String baseUrl;
    private final static String ticketEndpoint = "/tickets/";
    private String auth;
    private int vehId;
    private String keyLocation;
    private int ticketStatus;
    
    public TicketUpdateRequest( String baseUrl, String auth, int ticketId, int customerId, int eventId, int vehId,
    		String keyLoc, int status) {
        super( Ticket.class );
        
        String accessUrl = eventId > 0 ? "/events/" + eventId : "/customers/" + customerId;
        this.baseUrl = baseUrl + accessUrl + ticketEndpoint + ticketId + ".json";
        this.auth = auth;
        this.vehId = vehId;
        this.keyLocation = keyLoc;
        this.ticketStatus = status;
    }

    @Override
    public Ticket loadDataFromNetwork() throws IOException, JSONException {
        Ln.d( "Call web service " + baseUrl );
        GenericData ticketInfo = new GenericData();
        if(!keyLocation.isEmpty())
        	ticketInfo.put(JsonKeys.keyLocation, keyLocation);
        if(vehId > 0)
        	ticketInfo.put(JsonKeys.vehicleId, vehId);
        if(ticketStatus > TicketStatus.OPEN)
        	ticketInfo.put(JsonKeys.ticketStatus, ticketStatus);

        HttpRequest request = getHttpRequestFactory().buildPutRequest(new GenericUrl( baseUrl ) ,
        		new JsonHttpContent(new JacksonFactory(), ticketInfo));
        HttpHeaders headers = new HttpHeaders()
	        .setAccept("application/json")
	        .setContentType("application/json")
	        .setAuthorization(auth);
        request.setHeaders(headers);
        
        request.setParser( new JacksonFactory().createJsonObjectParser() );
        return request.execute().parseAs( getResultType() );
    }

}