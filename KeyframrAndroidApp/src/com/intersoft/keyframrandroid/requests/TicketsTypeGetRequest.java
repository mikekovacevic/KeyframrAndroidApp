/*-------------------------------------------------------------------------*/ 
/* Program Name : TicketsTypeRequest.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments :  Create a ticket types get request to server. 
/*-------------------------------------------------------------------------*/
package com.intersoft.keyframrandroid.requests;

import java.io.IOException;

import roboguice.util.temp.Ln;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.intersoft.keyframrandroid.models.json.TicketType;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

public class TicketsTypeGetRequest extends GoogleHttpClientSpiceRequest< TicketType[] > {

    private String baseUrl;
    private final static String endpoint = "/ticket_types.json";
    private String auth;
    
    public TicketsTypeGetRequest( String baseUrl, String auth ) {
        super( TicketType[].class );
        this.baseUrl = baseUrl + endpoint;
        this.auth = auth;
    }

    @Override
    public TicketType[] loadDataFromNetwork() throws IOException {
        Ln.d( "Call web service " + baseUrl );
        
        HttpRequest request = getHttpRequestFactory().buildGetRequest(new GenericUrl( baseUrl ));
        HttpHeaders headers = new HttpHeaders()
	        .setAccept("application/json")
	        .setContentType("application/json")
	        .setAuthorization(auth);
        request.setHeaders(headers);
        request.setParser( new JacksonFactory().createJsonObjectParser() );
        return request.execute().parseAs( getResultType() );
    }
    
}