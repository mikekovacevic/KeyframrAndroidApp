/*-------------------------------------------------------------------------*/ 
/* Program Name : CustomerCreateRequest.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments :  Creates a customer create request to server. 
/*-------------------------------------------------------------------------*/

package com.intersoft.keyframrandroid.requests;

import java.io.IOException;

import org.json.JSONException;

import roboguice.util.temp.Ln;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.GenericData;
import com.intersoft.keyframrandroid.models.json.Customer;
import com.intersoft.keyframrandroid.models.json.JsonKeys;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

public class CustomerCreateRequest extends GoogleHttpClientSpiceRequest< Customer > {

    private String baseUrl;
    private final static String endpoint = "/customers.json";
    private String auth;
    private String firstname;
    private String lastname;
    private String phoneno;
    
    public CustomerCreateRequest( String baseUrl, String auth, String firstname, String lastname, String phoneno ) {
        super( Customer.class );
        this.baseUrl = baseUrl + endpoint;
        this.auth = auth;
        this.firstname = firstname;
        this.lastname = lastname;
        this.phoneno = phoneno;
    }

    @Override
    public Customer loadDataFromNetwork() throws IOException, JSONException {
        Ln.d( "Call web service " + baseUrl );
        
        GenericData cusInfo = new GenericData();
        cusInfo.put(JsonKeys.firstName, this.firstname);
        cusInfo.put(JsonKeys.lastName, this.lastname);
        cusInfo.put(JsonKeys.lastName, this.phoneno);
        GenericData cus = new GenericData();
        cus.put(JsonKeys.customer, cusInfo);
        
        HttpRequest request = getHttpRequestFactory().buildPostRequest(new GenericUrl( baseUrl ) ,
        		new JsonHttpContent(new JacksonFactory(), cus));
        HttpHeaders headers = new HttpHeaders()
	        .setAccept("application/json")
	        .setContentType("application/json")
	        .setAuthorization(auth);
        request.setHeaders(headers);
        
        request.setParser( new JacksonFactory().createJsonObjectParser() );
        return request.execute().parseAs( getResultType() );
    }

}