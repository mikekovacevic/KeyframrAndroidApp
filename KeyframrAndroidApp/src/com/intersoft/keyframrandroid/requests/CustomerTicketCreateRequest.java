package com.intersoft.keyframrandroid.requests;

import java.io.IOException;

import org.json.JSONException;

import roboguice.util.temp.Ln;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.GenericData;
import com.intersoft.keyframrandroid.models.json.JsonKeys;
import com.intersoft.keyframrandroid.models.json.Ticket;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

public class CustomerTicketCreateRequest extends GoogleHttpClientSpiceRequest< Ticket > {

    private String baseUrl;
    private final static String endpoint = "/tickets.json";
    private String auth;
    private int ticketType;
    
    public CustomerTicketCreateRequest( String baseUrl, String auth, int ticketTypeId, int cusId ) {
        super( Ticket.class );
        this.baseUrl = baseUrl + "/customers/" + cusId + endpoint;
        this.auth = auth;
        this.ticketType = ticketTypeId;
    }

    @Override
    public Ticket loadDataFromNetwork() throws IOException, JSONException {
        Ln.d( "Call web service " + baseUrl );
        
        GenericData ticketInfo = new GenericData();
        ticketInfo.put(JsonKeys.ticketTypeId, this.ticketType);
        GenericData ticket = new GenericData();
        ticket.put(JsonKeys.ticket, ticketInfo);
        
        HttpRequest request = getHttpRequestFactory().buildPostRequest(new GenericUrl( baseUrl ) ,
        		new JsonHttpContent(new JacksonFactory(), ticket));
        HttpHeaders headers = new HttpHeaders()
	        .setAccept("application/json")
	        .setContentType("application/json")
	        .setAuthorization(auth);
        request.setHeaders(headers);
        
        request.setParser( new JacksonFactory().createJsonObjectParser() );
        return request.execute().parseAs( getResultType() );
    }

}