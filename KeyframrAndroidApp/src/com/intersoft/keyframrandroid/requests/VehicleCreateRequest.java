/*-------------------------------------------------------------------------*/ 
/* Program Name : VehicleCreateRequest.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments :  Creates a vehicle post request to server. 
/*-------------------------------------------------------------------------*/
package com.intersoft.keyframrandroid.requests;

import java.io.IOException;

import org.json.JSONException;

import roboguice.util.temp.Ln;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.GenericData;
import com.intersoft.keyframrandroid.models.json.JsonKeys;
import com.intersoft.keyframrandroid.models.json.Vehicle;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

public class VehicleCreateRequest extends GoogleHttpClientSpiceRequest< Vehicle > {

    private String baseUrl;
    private final static String endpoint = "/vehicles.json";
    private String auth;
    private String number;
    private String location;
    private String make;
    private String model;
    private String color;
    private String status = "";
    
    public VehicleCreateRequest( String baseUrl, String auth, String lpn, String lpc, String make, String model, String color) {
        super( Vehicle.class );
        this.baseUrl = baseUrl + endpoint;
        this.auth = auth;
        this.number = lpn;
        this.location = lpc;
        this.make = make;
        this.model = model;
        this.color = color;
    }

    @Override
    public Vehicle loadDataFromNetwork() throws IOException, JSONException {
        Ln.d( "Call web service " + baseUrl );
        
        GenericData vehInfo = new GenericData();
        vehInfo.put(JsonKeys.vehMake, this.make);
        vehInfo.put(JsonKeys.vehModel, this.model);
        vehInfo.put(JsonKeys.vehColor, this.color);
        vehInfo.put(JsonKeys.vehStatus, this.status);
        vehInfo.put(JsonKeys.vehLpNumber, this.number);
        vehInfo.put(JsonKeys.vehLpLocation, this.location);
        GenericData veh = new GenericData();
        veh.put(JsonKeys.vehicle, vehInfo);
        
        HttpRequest request = getHttpRequestFactory().buildPostRequest(new GenericUrl( baseUrl ) ,
        		new JsonHttpContent(new JacksonFactory(), veh));
        HttpHeaders headers = new HttpHeaders()
	        .setAccept("application/json")
	        .setContentType("application/json")
	        .setAuthorization(auth);
        request.setHeaders(headers);
        
        request.setParser( new JacksonFactory().createJsonObjectParser() );
        return request.execute().parseAs( getResultType() );
    }

}