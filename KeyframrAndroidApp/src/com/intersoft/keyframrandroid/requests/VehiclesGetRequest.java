/*-------------------------------------------------------------------------*/ 
/* Program Name : VehiclesGetRequest.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments :  Creates a vehicles get request to server. 
/*-------------------------------------------------------------------------*/

package com.intersoft.keyframrandroid.requests;

import java.io.IOException;

import org.json.JSONException;

import roboguice.util.temp.Ln;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.intersoft.keyframrandroid.models.json.Vehicle;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

public class VehiclesGetRequest extends GoogleHttpClientSpiceRequest< Vehicle[] > {

    private String baseUrl;
    private final static String endpoint = "/vehicles.json";
    private String auth;

    
    public VehiclesGetRequest( String baseUrl, String auth) {
        super( Vehicle[].class );
        this.baseUrl = baseUrl + endpoint;
        this.auth = auth;
    }

    @Override
    public Vehicle[] loadDataFromNetwork() throws IOException, JSONException {
        Ln.d( "Call web service " + baseUrl );
        
        HttpRequest request = getHttpRequestFactory().buildGetRequest(new GenericUrl( baseUrl ));
        HttpHeaders headers = new HttpHeaders()
	        .setAccept("application/json")
	        .setContentType("application/json")
	        .setAuthorization(auth);
        request.setHeaders(headers);
        
        request.setParser( new JacksonFactory().createJsonObjectParser() );
        return request.execute().parseAs( getResultType() );
    }

}
