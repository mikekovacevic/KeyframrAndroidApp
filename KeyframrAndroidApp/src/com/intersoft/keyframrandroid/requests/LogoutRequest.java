/*-------------------------------------------------------------------------*/ 
/* Program Name : LogoutRequest.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments :  Creates a logout request to server. 
/*-------------------------------------------------------------------------*/
package com.intersoft.keyframrandroid.requests;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.util.temp.Ln;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

public class LogoutRequest extends GoogleHttpClientSpiceRequest< JSONObject > {

    private String baseUrl;
    private final static String loginEndpoint = "/mobile/sessions.json";
    private String auth;
    
    public LogoutRequest( String baseUrl, String authToken ) {
        super( JSONObject.class );
        this.baseUrl = baseUrl + loginEndpoint;
        this.auth = authToken;
    }

    @Override
    public JSONObject loadDataFromNetwork() throws IOException {
        Ln.d( "Call web service " + baseUrl );
        
        HttpRequest request = getHttpRequestFactory().buildDeleteRequest(new GenericUrl( baseUrl ));

        HttpHeaders headers = new HttpHeaders().setAccept("application/json")
        		.setContentType("application/json")
        		.setAuthorization(auth);
        request.setHeaders(headers);

        String unparsed = request.execute().parseAsString();

        try {
			return new JSONObject(unparsed);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
    }


}