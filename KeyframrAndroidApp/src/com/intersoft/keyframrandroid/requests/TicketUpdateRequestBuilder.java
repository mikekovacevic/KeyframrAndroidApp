/*-------------------------------------------------------------------------*/ 
/* Program Name : TicketUpdateRequestBuilder.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments :  Builder class for the ticket update request. Calling build() without setting any of the properties 
 * results in updating the ticket for an anonymous customer, for an anonymous vehicle
/*-------------------------------------------------------------------------*/
package com.intersoft.keyframrandroid.requests;

import com.intersoft.keyframrandroid.utils.TicketStatus;

public class TicketUpdateRequestBuilder {
	// customer id of 1 is an anonymous customer
	private int cusId = 1;
	private int eveId = -1;
	// vehicle id of -1 tells the TicketUpdateRequest class to ignore the vehicle id when updating the ticket (anonymous vehicle)
	private int vehId = -1;
	private String keyLoc = "";
	private int status = TicketStatus.OPEN;
	
	public TicketUpdateRequest build(String baseUrl, String auth, int ticketId){
		return new TicketUpdateRequest(baseUrl, auth, ticketId, cusId, eveId, vehId, keyLoc, status);
	}
	
	public TicketUpdateRequestBuilder setCustomerId(int id){
		this.cusId = id;
		return this;
	}
	
	public TicketUpdateRequestBuilder setEventId(int id){
		this.eveId = id;
		return this;
	}
	
	public TicketUpdateRequestBuilder setVehicleId(int id){
		this.vehId = id;
		return this;
	}
		
	public TicketUpdateRequestBuilder setVehicleKeyLocation(String loc){
		this.keyLoc = loc;
		return this;
	}
	
	public TicketUpdateRequestBuilder setStatus(int stat){
		this.status = stat;
		return this;
	}
}
