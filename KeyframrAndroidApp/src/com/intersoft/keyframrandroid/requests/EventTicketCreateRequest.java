/*-------------------------------------------------------------------------*/ 
/* Program Name : EventTicketCreateRequest.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments :  Create an event ticket post request to server. 
/*-------------------------------------------------------------------------*/
package com.intersoft.keyframrandroid.requests;

import java.io.IOException;

import org.json.JSONException;

import roboguice.util.temp.Ln;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.GenericData;
import com.intersoft.keyframrandroid.models.json.JsonKeys;
import com.intersoft.keyframrandroid.models.json.Ticket;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

public class EventTicketCreateRequest extends GoogleHttpClientSpiceRequest< Ticket > {

    private String baseUrl;
    private final static String endpoint = "/tickets.json";
    private String auth;
    private int ticketType;
    private int eventConId;
    
    public EventTicketCreateRequest( String baseUrl, String auth, int eventId, int ticketTypeId, int eventConstraintId ) {
        super( Ticket.class );
        this.baseUrl = baseUrl + "/events/" + eventId + endpoint;
        this.auth = auth;
        this.ticketType = ticketTypeId;
        this.eventConId = eventConstraintId;
    }

    @Override
    public Ticket loadDataFromNetwork() throws IOException, JSONException {
        Ln.d( "Call web service " + baseUrl );
        

        GenericData ticketInfo = new GenericData();
        ticketInfo.put(JsonKeys.ticketTypeId, this.ticketType);
        ticketInfo.put(JsonKeys.eventConstraintId, eventConId);
        GenericData ticket = new GenericData();
        ticket.put(JsonKeys.ticket, ticketInfo);
        
        HttpRequest request = getHttpRequestFactory().buildPostRequest(new GenericUrl( baseUrl ) ,
        		new JsonHttpContent(new JacksonFactory(), ticket));
        HttpHeaders headers = new HttpHeaders()
	        .setAccept("application/json")
	        .setContentType("application/json")
	        .setAuthorization(auth);
        request.setHeaders(headers);
        
        request.setParser( new JacksonFactory().createJsonObjectParser() );
        return request.execute().parseAs( getResultType() );
    }

}