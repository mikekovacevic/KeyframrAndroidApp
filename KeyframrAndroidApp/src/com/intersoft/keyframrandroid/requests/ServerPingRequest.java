/*-------------------------------------------------------------------------*/ 
/* Program Name : ServerPingRequest.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments : Pings the Server. 
/*-------------------------------------------------------------------------*/
package com.intersoft.keyframrandroid.requests;

import java.io.IOException;

import roboguice.util.temp.Ln;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;
import com.octo.android.robospice.retry.DefaultRetryPolicy;
import com.octo.android.robospice.retry.RetryPolicy;

public class ServerPingRequest extends GoogleHttpClientSpiceRequest< String > {

    private String baseUrl;
    private final static String endpoint = "/mobile/ping.json";

    
    public ServerPingRequest( String baseUrl ) {
        super( String.class );
        this.baseUrl = baseUrl + endpoint;
    }

    @Override
    public String loadDataFromNetwork() throws IOException {
        Ln.d( "Call web service " + baseUrl );
        // Override default retry policy (two retries), and set to no retry
        RetryPolicy retryPolicy = new DefaultRetryPolicy(0, 0000, (float) 0.0);
        setRetryPolicy(retryPolicy);
        HttpRequest request = getHttpRequestFactory().buildGetRequest(new GenericUrl( baseUrl ));
        HttpHeaders headers = new HttpHeaders()
	        .setAccept("application/json")
	        .setContentType("application/json");
        
        request.setHeaders(headers);

        return request.execute().parseAsString();

    }

}