/*-------------------------------------------------------------------------*/ 
/* Program Name : LoginRequest.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments :  creates a login request to server. 
/*-------------------------------------------------------------------------*/
package com.intersoft.keyframrandroid.requests;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.util.temp.Ln;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.GenericData;
import com.intersoft.keyframrandroid.models.json.JsonKeys;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

public class LoginRequest extends GoogleHttpClientSpiceRequest< JSONObject > {

    private String baseUrl;
    private final static String loginEndpoint = "/mobile/sessions.json";
    private String name;
    private String pass;
    
    public LoginRequest( String baseUrl, String username, String password ) {
        super( JSONObject.class );
        this.baseUrl = baseUrl + loginEndpoint;
        this.name = username;
        this.pass = password;
    }

    @Override
    public JSONObject loadDataFromNetwork() throws IOException {
        Ln.d( "Call web service " + baseUrl );

        GenericData usercreds = new GenericData();
        usercreds.put(JsonKeys.LoginName, this.name);
        usercreds.put(JsonKeys.loginPassword, this.pass);
        GenericData userobj = new GenericData();
        userobj.put(JsonKeys.loginUser, usercreds);
        
        HttpRequest request = getHttpRequestFactory().buildPostRequest(new GenericUrl( baseUrl ) ,
        		new JsonHttpContent(new JacksonFactory(), userobj));
        HttpHeaders headers = new HttpHeaders().setAccept("application/json").setContentType("application/json");
        
        request.setHeaders(headers);

        String unparsed = request.execute().parseAsString();

        try {
			return new JSONObject(unparsed);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
    }

}