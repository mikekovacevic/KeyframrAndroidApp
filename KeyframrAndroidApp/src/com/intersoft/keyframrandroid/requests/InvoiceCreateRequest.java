/*-------------------------------------------------------------------------*/ 
/* Program Name : CustomerCreateRequest.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments :  Creates a customer create request to server. 
/*-------------------------------------------------------------------------*/

package com.intersoft.keyframrandroid.requests;

import java.io.IOException;

import org.json.JSONException;

import roboguice.util.temp.Ln;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.GenericData;
import com.intersoft.keyframrandroid.models.json.Customer;
import com.intersoft.keyframrandroid.models.json.JsonKeys;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;
import com.octo.android.robospice.retry.DefaultRetryPolicy;
import com.octo.android.robospice.retry.RetryPolicy;

public class InvoiceCreateRequest extends GoogleHttpClientSpiceRequest< String > {

    private String baseUrl;
    private final static String endpoint = "/invoices.json";
    private String auth;
    private float amount;
    
    public InvoiceCreateRequest( String baseUrl, String auth, int cusId, int ticketId, float amount ) {
        super( String.class );
        this.baseUrl = baseUrl + "/customers/" + cusId 
        		+ "/tickets/" + ticketId + endpoint;
        this.auth = auth;
        this.amount = amount;
    }

    @Override
    public String loadDataFromNetwork() throws IOException, JSONException {
        Ln.d( "Call web service " + baseUrl );
        
        // Override default retry policy (two retries), and set to no retry
        RetryPolicy retryPolicy = new DefaultRetryPolicy(0, 0000, (float) 0.0);
        setRetryPolicy(retryPolicy);
        
        GenericData invoiceInfo = new GenericData();
        invoiceInfo.put(JsonKeys.amount, this.amount);
        
        GenericData invoice = new GenericData();
        invoice.put(JsonKeys.invoice, invoiceInfo);
        
        HttpRequest request = getHttpRequestFactory().buildPostRequest(new GenericUrl( baseUrl ) ,
        		new JsonHttpContent(new JacksonFactory(), invoice));
        HttpHeaders headers = new HttpHeaders()
	        .setAccept("application/json")
	        .setContentType("application/json")
	        .setAuthorization(auth);
        request.setHeaders(headers);
        
        return request.execute().parseAsString();
    }

}