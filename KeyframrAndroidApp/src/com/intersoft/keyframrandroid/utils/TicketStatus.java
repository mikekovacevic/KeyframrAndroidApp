/*-------------------------------------------------------------------------*/ 
/* Program Name : TicketStatus.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 10/12/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments :  Utility class for defining ticket status constants. These values are defined at the server end
 * as well;
 */
/*-------------------------------------------------------------------------*/

package com.intersoft.keyframrandroid.utils;

public class TicketStatus {
	public final static int OPEN = 0;
	public final static int VEHICLE_REQUESTED = 1;
	public final static int VEHICLE_RETURNED = 2;
	
	private TicketStatus(){
	}
}
