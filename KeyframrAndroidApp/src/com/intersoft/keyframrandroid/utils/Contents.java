package com.intersoft.keyframrandroid.utils;


public final class Contents {
    private Contents() {
    }

    public static final class Type {
        public static final String TICKET = "TICKET_TYPE";
        public static final String VEHICLE = "VEHICLE_TYPE";
        public static final String CUSTOMER = "CUSTOMER_TYPE";
        public static final String EVENT = "EVENT_TYPE";

        private Type() {
        }
    }
    
    public static final class Header {
    	   // headers that are prepended to the data of a qr/bar code.
           
           public static final String TICKET = "ticket_id";
           public static final String VEHICLE = "vehicle_id";
           public static final String CUSTOMER = "customer_id";
           public static final String EVENT = "event_id";
           public static final String KEYS = "vehicle_keys";
           private Header() {
           }
       }
   
}