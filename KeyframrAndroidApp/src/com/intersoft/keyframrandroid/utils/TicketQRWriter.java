/*-------------------------------------------------------------------------*/ 
/* Program Name : TicketQRWriter.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments : Class to encode the QR code and return a bitmap of the QR.
/*-------------------------------------------------------------------------*/

package com.intersoft.keyframrandroid.utils;

import android.graphics.Bitmap;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

public class TicketQRWriter {
	private QRCodeEncoder qrEncoder;
	private Bitmap bmp;
	public TicketQRWriter(String data, int dimension) {
        this.qrEncoder = new QRCodeEncoder(data, null,
    	        Contents.Type.TICKET, BarcodeFormat.QR_CODE.toString(), dimension);
    }
	
	public Bitmap encodeBitmap(){
		try {
		    Bitmap bitmap = this.qrEncoder.encodeAsBitmap();
		    this.bmp = bitmap;
		    return bitmap;

		} catch (WriterException e) {
		    e.printStackTrace();
		    return null;
		}
	}
	
	public Bitmap getBitmap(){
		return this.bmp;
	}
}
