/*-------------------------------------------------------------------------*/ 
/* Program Name : QRContents.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments : Utility class to format the QR code contents
/*-------------------------------------------------------------------------*/

package com.intersoft.keyframrandroid.utils;

import com.intersoft.keyframrandroid.utils.Contents.Header;

public class QRContents {
	private String ticket;
	private String cus = "";
	private String eve = "";
	private String veh = "";
	private String keysLoc = "";
	public QRContents(int ticketId, int cusId, int eveId, int vehId){
		this.ticket = Header.TICKET + "[" + ticketId + "]";
		if(cusId > -1)
			this.cus = Header.CUSTOMER + "[" + cusId + "]";
		else if(eveId > -1)
			this.eve = Header.EVENT + "[" + eveId + "]";
		else if(vehId > -1)
			this.veh = Header.VEHICLE + "[" + vehId + "]";
		else 
			this.keysLoc = Header.KEYS;
	}
	
	@Override
	public String toString(){
		return "keyframr:"+ticket+cus+eve+veh+keysLoc;
	}
}
