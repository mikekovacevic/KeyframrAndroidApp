/*-------------------------------------------------------------------------*/ 
/* Program Name : ConnectThread.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments :  Utility class to help pair to a bluetooth device.  
/*-------------------------------------------------------------------------*/
package com.intersoft.keyframrandroid.utils;

import java.io.IOException;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.widget.Toast;

public class PairThread extends Thread {
    private final BluetoothSocket mmSocket;
    private final BluetoothDevice mmDevice;
    private final BluetoothAdapter bluetooth;
    private final Activity context;
    private static UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private boolean paired = false;
    public PairThread(Activity activity, BluetoothDevice device, BluetoothAdapter adapter) {
        // Use a temporary object that is later assigned to mmSocket,
        // because mmSocket is final
        BluetoothSocket tmp = null;
        mmDevice = device;
        bluetooth = adapter;
        context = activity;

        // Get a BluetoothSocket to connect with the given BluetoothDevice
        try {
            // MY_UUID is the app's UUID string, also used by the server code
            tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
        } catch (IOException e) { }
        mmSocket = tmp;
    }
 
    public void run() {
        // Cancel discovery because it will slow down the connection
        bluetooth.cancelDiscovery();
        paired = false;
        try {
            // Connect the device through the socket. This will block
            // until it succeeds or throws an exception
            mmSocket.connect();
            paired = true;
            Toast.makeText(context, "Paired with "+mmDevice.getName(), Toast.LENGTH_SHORT).show();
        } catch (IOException connectException) {
            // Unable to connect; close the socket and get out
            try {
                mmSocket.close();
            } catch (IOException closeException) { }
            Toast.makeText(context, "Connect failed, Try again.", Toast.LENGTH_SHORT).show();
            connectException.printStackTrace();
            return;
        }
 
    }
    public boolean isPaired(){
    	return paired;
    }
    /** Will cancel an in-progress connection, and close the socket */
    public void cancel() {
        try {
            mmSocket.close();
        } catch (IOException e) { }
    }
    
    public BluetoothSocket getSocket(){
    	return this.mmSocket;
    }
    
    public BluetoothDevice getDevice(){
    	return this.mmDevice;
    }
}
