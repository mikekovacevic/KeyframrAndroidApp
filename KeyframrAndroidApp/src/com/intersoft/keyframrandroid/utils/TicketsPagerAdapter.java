/*-------------------------------------------------------------------------*/ 
/* Program Name : TicketsPagerAdapter.java */ 
/* Designed by : Faisal Alqadi */ 
/* Created by : Faisal Alqadi */ 
/* Creation date : 12/11/2013 01:59 PM */ 
/* Version number : 1.0 */ 
/* Author comments : Fragment Pager Adapter to handle the Customer Ticket and Event Ticket Fragments 
/*-------------------------------------------------------------------------*/

package com.intersoft.keyframrandroid.utils;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.intersoft.keyframrandroid.fragment.CustomerTicketFragment;
import com.intersoft.keyframrandroid.fragment.EventTicketFragment;
public class TicketsPagerAdapter extends FragmentPagerAdapter{
	final int PAGE_COUNT = 2;
	 
    /** Constructor of the class */
    public TicketsPagerAdapter(FragmentManager fm) {
        super(fm);
    }
 
    /** This method will be invoked when a page is requested to create */
    @Override
    public Fragment getItem(int arg0) {
        Bundle data = new Bundle();
        switch(arg0){          
            /** Customer ticket tab is selected */
            case 0:
            	CustomerTicketFragment ctFragment = new CustomerTicketFragment();
                data.putInt("current_page", arg0+1);
                ctFragment.setArguments(data);
                return ctFragment;
                
            /** Event ticket tab is selected */
            case 1:
            	EventTicketFragment etFragment = new EventTicketFragment();
                data.putInt("current_page", arg0+1);
                etFragment.setArguments(data);
                return etFragment;
        }
        return null;
    }
 
    /** Returns the number of pages */
    @Override
    public int getCount() {
    	return PAGE_COUNT;
    }
}
