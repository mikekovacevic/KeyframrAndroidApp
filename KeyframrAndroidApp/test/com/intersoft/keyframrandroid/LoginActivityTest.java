package com.intersoft.keyframrandroid;

import static org.fest.assertions.api.ANDROID.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.robolectric.Robolectric.shadowOf;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowIntent;

import com.intersoft.keyframrandroid.LoginActivity;

import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;


@RunWith(RobolectricTestRunner.class)
public class LoginActivityTest {
    private LoginActivity activity;
    private Button loginButton;
    private EditText userView;
    private EditText passwordView;
    private static String VALIDUSER = "fai";
    private static String VALIDPASS = "password";
    private static String INVALIDUSER = "";
    private static String SHORTPASS = "12";
    private static String EMPTYPASS = "";
    
    @Before
    public void setUp() throws Exception {
    	Robolectric.getFakeHttpLayer().interceptHttpRequests(false);
        activity = Robolectric.buildActivity(LoginActivity.class).create().get();
/*        activity = new LoginActivity();
        activity.onCreate(null);*/
    	
    	loginButton = (Button) activity.findViewById(R.id.sign_in_button);
        userView = (EditText) activity.findViewById(R.id.email);
        passwordView = (EditText) activity.findViewById(R.id.password);
    }


	@Test
    public void enteringCorrectCredentialsAndPressingButtonLogsIn() throws Exception {
        userView.setText(VALIDUSER);
        passwordView.setText(VALIDPASS);
		loginButton.performClick();

        ShadowActivity shadowActivity = shadowOf(activity);
        Intent startedIntent = shadowActivity.getNextStartedActivity();
        ShadowIntent shadowIntent = shadowOf(startedIntent);
        assertThat(shadowIntent.getComponent().getClassName(), equalTo(MainActivity.class.getName()));
    }

    @Test
    public void pressingButtonWithNoUserGivesAnError() throws Exception {
    	userView.setText(INVALIDUSER);
        passwordView.setText(VALIDPASS);
		loginButton.performClick();

        assertThat(userView).hasError();
    }

    @Test
    public void pressingButtonWithEmptyPasswordGivesAnError() throws Exception {
    	userView.setText(INVALIDUSER);
        passwordView.setText(EMPTYPASS);
		loginButton.performClick();

        assertThat(passwordView).hasError();
    }

    @Test
    public void pressingButtonWithShortPasswordGivesAnError() throws Exception {
    	userView.setText(INVALIDUSER);
        passwordView.setText(SHORTPASS);
		loginButton.performClick();

        assertThat(passwordView).hasError();
    }
}